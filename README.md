# Broadside
## Downloads
It is possible to download built **Broadside** images [here](https://drive.google.com/drive/folders/1qmXR3GzaOHLCaKpV2URoTs3wqD6nRx46?usp=sharing).

## About
**Broadside** is a distributed supervised feature selection and interaction mining 
software. 
It can work on high-dimensional datasets containing millions of features as well as on smaller 
sets of well chosen clinical or PCR variables. 
Both numerical and categorical input variables are supported. 
**Broadside** can be ran on a desktop as well as on a clusters containing hundreds of processors.
It was designed primarily for high-throughput biological experiments and clinical studies,
but can be applied to any structured supervised classification, regression or survival analysis problem.

## Interaction Mining
Through interactions, we understand arrangements of structured dataset variables that result 
in an improved supervised Machine Learning model score. 
In some cases, interacting variables can have very limited predictive power of their own, 
while yielding high "accuracy" models when being included together. 


Take for example the following classification problem in a 2-D continuous feature space: 

![XOR/Chessboard Problem](broadside_directories/images/XOR.png)

Neither **Variable 1** nor **Variable 2** can separate the 2 classes alone, 
while they can be used together to train a "perfect" classifer.  

As real-life example: the plot below shows an interaction detected in a neuroblastoma survival analysis 
study between expression of an intron junction in PDSS1 -  one of the enzymes responsible for coenzyme Q10 synthesis 
and a clinical risk factor. While low expressions of this gene result in longer expected event-free survival time, 
the difference becomes dramatic for low risk patients:

![Interaction in a Survival Analysis Study](broadside_directories/images/interaction.png)

**Broadside** detects and **explicitly reports** these pairs of interacting variables
in order for them to be studied and understood by domain experts. 
Recommended way of viewing these interactions is through analysis of so called **Interaction Networks**.
A basic R-based Shiny Server dashboard serving this purpose is provided 
in the separate [Tackle](https://bitbucket.org/broadsiderd/tackle)  project: 
 
![Tackle](broadside_directories/images/Tackle.png)  

Internally, Broadside trains and tests up to millions of supervised Machine Learning models on random
subsets of the original dataset - both in terms of features and observations.  
As a workhorse model, it relies on variants of decision trees (C4.5 classificaiton trees, regression trees, survival trees)
because of their robustness, embedded feature selection properties and speed. 
It is generally model agnostic however, so it is possible to plug-in other models such as SVMs, neural networks etc.

## Requirements
Broadside is designed as platform independent software. It is written in Scala and Java and makes use of 
Lightbend's [Akka](https://github.com/akka/akka) for asynchronous distributed computing. 
[JSAT](https://github.com/EdwardRaff/JSAT) is used as core Machine Learning library.  

## How to get Broadside
You can get the latest tested version of software and documentation [here](https://bitbucket.org/broadsiderd/broadside/downloads/).
It is also available on my [University's team webpage](http://zaed.aei.polsl.pl/index.php/pl/oprogramowanie-zaed). 
You can also download contents of this repository containing the Intellij IDEA project.
Broadside should also be available through Maven the in near future.

## How to operate Broadside
Broadside is implemented as asynchronous distributed software with nearly linear scalability with respect to the
number of available processors. Nodes are divided into **Master** and **Booster** nodes.
Only one **Master** node can be present in a cluster. 
An optional number of **Booster** nodes can be dynamically attached to **Master** in order to speed-up computations.

Currently the most straightforward way of launching **Broadside** is to use the provided Windows and Linux
launch scripts in order to run calculations in "distributed batch mode". 
It is also possible to expose the **Master** node through Java RMI, this is not extensively tested though.
Finally, it is technically possible to use the **Broadside** jar file as a library
by passing an instance of **com.typesafe.config.Config** class to the constructor of 
**broadside.control.Listener** and then interacting with it through **broadside.api.RemoteAPI**.

Documentation for **Broadside** is provided [here](broadside_directories/doc/broadside_1.1.1.pdf).

## Some context
I wrote **Broadside** as the implementation section of my [PhD thesis](broadside_directories/doc/Broadside_PhD_thesis.pdf).
This is research work, so multiple approaches were tested over time, which is reflected by some blind paths which
can be found in the code.  
I also used this project as an opportunity to do some **Scala** coding. 
This is my first **Scala** project, so an experienced programmer might find some of my ideas inadequate. 
I'll be grateful for sending constructive critique over to lukasz.krol@polsl.pl or my private address if you have it:)

## Citations
Flagship paper is on its way.  
If you use **Broadside** in your research, please cite the following conference papers:  

    @InProceedings{10.1007/978-3-319-34099-9_35,
        author="Krol, Lukasz",
        title="Distributed Monte Carlo Feature Selection: Extracting Informative Features Out of Multidimensional Problems with Linear Speedup",
        booktitle="Beyond Databases, Architectures and Structures. Advanced Technologies for Data Mining and Knowledge Discovery",
        year="2016",
        publisher="Springer International Publishing",
        pages="463--474",
        isbn="978-3-319-34099-9"
    }

    @InProceedings{10.1007/978-3-319-60816-7_15,
        author="Krol, Lukasz and Polanska, Joanna",
        title="Multidimensional Feature Selection and Interaction Mining with Decision Tree Based Ensemble Methods",
        booktitle="11th International Conference on Practical Applications of Computational Biology {\&} Bioinformatics",
        year="2017",
        publisher="Springer International Publishing",
        pages="118--125",
        isbn="978-3-319-60816-7"
    }

## License & Commercial Use
Broadside is distributed under the GPL-3.0 license.  
If you require consultation on usage in commercial scenarios, please feel free to contact me.