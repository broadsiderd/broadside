/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.statistics

class SurvivalArrays(
                      observationTimes: Array[java.lang.Double]
                    ) {
  val nTimepoints = observationTimes.map( v => math.abs(v)).distinct.length

  var timepointIndices = new Array[Int](observationTimes.length)

  val deaths: Array[Double] = new Array[Double](nTimepoints)
  val quits: Array[Double] = new Array[Double](nTimepoints)
  val atRisk: Array[Double] = new Array[Double](nTimepoints)
  val hazard: Array[Double] = new Array[Double](nTimepoints)


  var numberOfDeaths: Int = 0

  var observationTimesOriginalSequence = observationTimes.
    zipWithIndex.
    sortWith( (v1,v2) => math.abs(v1._1) < math.abs(v2._1)).
    map(
      v => (v._1,v._2)
    )

  timepointIndices(0) = 0
  if (observationTimesOriginalSequence(0)._1 > 0) {
    deaths(0) += 1
    numberOfDeaths += 1
  } else
    quits(0) += 1

  var timepoint = 0
  for (i <- 1 to (observationTimes.length-1)){
    if (math.abs(observationTimesOriginalSequence(i-1)._1) < math.abs(observationTimesOriginalSequence(i)._1)){
      timepoint += 1
    }
    timepointIndices(i) = timepoint

    if (observationTimesOriginalSequence(i)._1 > 0) {
      deaths(timepoint) += 1
      numberOfDeaths += 1
    } else
      quits(timepoint) += 1
  }

  atRisk(0) = observationTimes.length
  if (atRisk(0)>0)
    hazard(0) = deaths(0)/atRisk(0)

  for (i <- 1 to (nTimepoints-1)){
    atRisk(i) = atRisk(i-1) - deaths(i-1) - quits(i-1)
    if (atRisk(i)>0)
      hazard(i) = deaths(i) / atRisk(i)
  }



  timepointIndices = timepointIndices.zip(
    observationTimesOriginalSequence
  ).sortWith(
    (v1,v2) => v1._2._2 < v2._2._2
  ).map(
    v => v._1
  )

  def getMortality(observationTimesAll: Array[java.lang.Double]): Double = {
    val indexedHazard = observationTimesOriginalSequence.map(
      v => math.abs(v._1)
    ).distinct.sorted.zip(
      hazard
    ).toMap

    var mortality = 0.0
    var cumulatedHazard = 0.0

    for (i <- 1 to observationTimesAll.size){
      cumulatedHazard += indexedHazard.getOrElse(observationTimesAll(i-1),0.0)
      mortality += cumulatedHazard
    }

   mortality
  }
}
