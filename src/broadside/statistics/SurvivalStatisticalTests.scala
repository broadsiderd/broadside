/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.statistics

import java.util

import Jama.Matrix
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

import scala.collection.mutable

/**
  * Created by luk on 3/12/17.
  */
object SurvivalStatisticalTests {
  val descriptiveStatistics = new DescriptiveStatistics

  val minPv = 1e-6

  def getNumberOfDeaths(datapoints: util.List[java.lang.Double]): Int = {
    var deaths = 0

    val it = datapoints.iterator()

    while (it.hasNext){
      if (it.next()>0){
        deaths += 1
      }
    }

    deaths
  }

  def calculateMortality(eventsIn: util.List[java.lang.Double], timesIn: Array[java.lang.Double]) = {
    import scala.collection.mutable.HashMap;

    val events = new HashMap[Double,(Int,Int)]
    val inIt = eventsIn.iterator()
    var event: Double = 0.0
    var oldValue: (Int,Int) = null

    while (inIt.hasNext){
      event = inIt.next
      if (events.contains(Math.abs(event))){
        oldValue = events.get(Math.abs(event)).get
        if (event>0){
          events += ((event,(oldValue._1 + 1,oldValue._2)))
        } else {
          events += ((-event,(oldValue._1,oldValue._2 + 1)))
        }
      } else {
        if (event>0){
          events += ((event,(1,0)))
        } else {
          events += ((-event,(0,1)))
        }
      }
    }



    var atRisk = eventsIn.size()
    var eventx: (Int,Int) = events.getOrElse(timesIn(0),((0,0)))
    var mortality = 0.0
    var cumulatedHazard = 0.0

    //cumulated hazard function
    for (i <- 1 to timesIn.size){
      eventx = events.getOrElse(timesIn(i-1),((0,0)))
      cumulatedHazard += (if (atRisk>0) eventx._1.toDouble / atRisk.toDouble else 0.0)
      mortality += cumulatedHazard
      atRisk -= (eventx._1 + eventx._2)
    }

    //mortality
    mortality
  }

  def petoPrenticeSeriesFast(
                              events: Array[java.lang.Double],
                              sa: SurvivalArrays4PetoPrentice,
                              from: Int,
                              to: Int
                            ): (Array[java.lang.Double],Array[java.lang.Double]) = {



    val deaths0 = new Array[Double](sa.deaths.length)
    val quits0 = new Array[Double](sa.quits.length)
    val atRisk0 = new Array[Double](sa.atRisk.length)
    val expectedDeaths0 = new Array[Double](sa.deaths.length)

    var nominator: Double = 0
    var denominator: Double = 0

    val Q = new Array[java.lang.Double](events.length-1)

    for (i <- 1 to (to) ){

      if (events(i-1) < 0){
        quits0(math.abs(sa.timepointIndices(i-1))) += 1
      }
      else {
        deaths0(math.abs(sa.timepointIndices(i-1))) += 1
      }

      if (i < from)
        Q(i-1) = Double.NegativeInfinity
      else if (i >= from){

        atRisk0(0) = deaths0.sum + quits0.sum
        expectedDeaths0(0) = atRisk0(0) * sa.hazard(0)

          for (j <- 1 to (sa.nTimepoints-1)){
            atRisk0(j) = atRisk0(j-1) - deaths0(j-1) - quits0(j-1)
            expectedDeaths0(j) = atRisk0(j) * sa.hazard(j)
          }


        denominator = 0.0
        nominator = 0.0
        for (j <- 1 to (sa.nTimepoints)){
          nominator += ( sa.weights(j-1)*(deaths0(j-1) - expectedDeaths0(j-1)) )
          denominator += ( atRisk0(j-1) * (sa.atRisk(j-1) - atRisk0(j-1)) * sa.denominatorConst(j-1) )
        }

        if (denominator == 0)
          Q(i-1) = Double.NegativeInfinity
        else
          Q(i-1) = math.pow(nominator,2) / denominator

      }
    }

    for (i <- (to+1) to (events.size-1)){
      Q(i-1) = Double.NegativeInfinity
    }

    val dist = new jsat.distributions.ChiSquared(1)
    var pv: Double = Double.PositiveInfinity
    val pvals = new Array[java.lang.Double](Q.size)

    for (i <- (1 to Q.size)){
      if (Q(i-1) == Double.NegativeInfinity)
        pvals(i-1) = Double.PositiveInfinity
      else {
        pvals(i-1) = scala.math.max(1 - dist.cdf(Q(i-1)), minPv)
      }
    }

    (Q,pvals)
  }


  def petoPrentice(argsRaw: java.util.LinkedList[java.util.LinkedList[java.lang.Double]]): (java.lang.Double,java.lang.Double) = {
    val args = new java.util.LinkedList[java.util.LinkedList[java.lang.Double]]

    for (i <- 1 to argsRaw.size()){
      if (argsRaw.get(i-1).size()>0){
        args.add(argsRaw.get(i-1))
      }
    }

    //if (args.size()<2) return 1.0

    val N = args.size

    val events = new mutable.HashMap[(Double,Int),(Int,Int)]()

    (1 to N).map(
      groupIdx => {
        val it = args.get(groupIdx-1).iterator()

        while(it.hasNext){
          val t = it.next()
          val tabs = math.abs(t);

          val record = events.get(tabs,groupIdx)

          if (record.isEmpty){
            if (t>0) events += (((tabs,groupIdx),(1,0)))
            else events += (((tabs,groupIdx),(0,1)))
          } else {
            if (t>0) events += (((tabs,groupIdx),(record.get._1 + 1,record.get._2)))
            else events += (((tabs,groupIdx),(record.get._1,record.get._2 + 1)))
          }



        }
      }
    )

    val times = events.keys.map( _._1).toList.distinct.sorted.toArray
    val deaths = new Array[Array[Double]](N+1)
    val atRisk = new Array[Array[Double]](N+1)


    for (i <- 0 to N){
      deaths(i) = new Array[Double](times.length)
      atRisk(i) = new Array[Double](times.length)
    }


    var populations = (1 to N).map( v => 0 ).toArray

    for (tidx <- times.length to 1 by -1){
      for (g <- 1 to N) {
        val t = times(tidx - 1)
        val record = events.get(t, g)
        if (record.isEmpty) {
          deaths(g)(tidx - 1) = 0
        } else {
          deaths(g)(tidx - 1) = record.get._1
          deaths(0)(tidx - 1) += record.get._1
          populations(g-1) += (record.get._1 + record.get._2)
        }
        atRisk(g)(tidx - 1) = populations(g-1)
        atRisk(0)(tidx - 1) += populations(g-1)
      }




    }

    val expectedDeaths = new Array[Array[Double]](N+1)
    for (g <- 1 to N){
      expectedDeaths(g) = (atRisk(0),atRisk(g),deaths(0)).zipped.toList.map(v => v._3.toDouble * (v._2.toDouble / v._1.toDouble)).toArray
    }

    var km = 1.0
    val weight = new Array[Double](times.length)
    for (tidx <- 1 to times.length){
      weight(tidx-1) = km * ((atRisk(0)(tidx-1) )/(atRisk(0)(tidx-1) +1))
      km = km * ( (atRisk(0)(tidx-1) - deaths(0)(tidx-1) + 1 ) / (atRisk(0)(tidx-1) + 1 ))
      weight(tidx-1) = km
    }




    var outer = new Matrix(N-1,1)
    var inner = new Matrix(N-1,N-1)


    val Wi = new Matrix(N-1,N-1)
    val Vi = new Matrix(N-1,N-1)
    val de = new Matrix(N-1,1)

    for (tidx <- 1 to times.length){
      val ni = atRisk(0)(tidx-1)
      if (ni>1) {
        val di = deaths(0)(tidx-1)
        val multiplier = (di*(ni-di))/(ni*ni*(ni-1.0))

        for (i <- 1 to N-1){
          Wi.set(i-1,i-1,weight(tidx-1))
          //Wi.set(i-1,i-1,1.0)
          de.set(i-1,0,deaths(i)(tidx-1) - expectedDeaths(i)(tidx-1))

          val nki = atRisk(i)(tidx-1)

          for (j <- 1 to N-1){

            if(j==i){
              Vi.set(i-1,j-1,nki * (ni-nki) * multiplier)
            } else {
              Vi.set(i-1,j-1,- nki * (atRisk(j)(tidx-1)) * multiplier)
            }
          }
        }


        outer.plusEquals(Wi.times(de))
        inner.plusEquals(Wi.times(Vi).times(Wi))
      }


    }

    //var pval = 0.0
    var Q = Double.NegativeInfinity
    try {
      Q = outer.transpose.times(inner.inverse).times(outer).get(0, 0)
      if (Q.isNaN || Q==0)
        Q = Double.NegativeInfinity
    }
    catch {
      case e: Exception => Q = Double.NegativeInfinity
    }


    val dist = new jsat.distributions.ChiSquared(N-1)
    val pv = if (Q == Double.NegativeInfinity) Double.PositiveInfinity else 1 - dist.cdf(Q)

    (Q,scala.math.max(pv,minPv))
  }

  def robustZScore(x: Double, values: Array[Double]): Double = {



    descriptiveStatistics.clear
    values.map( descriptiveStatistics.addValue( _ ))



    val med = descriptiveStatistics.getPercentile(50)

    descriptiveStatistics.clear
    values.map( v=> descriptiveStatistics.addValue(  math.abs(v - med)   ))
    val denominator = descriptiveStatistics.getPercentile(50)


    val rzs = if (denominator==0) 0.0
    else (x - med)/denominator

    rzs
  }



}
