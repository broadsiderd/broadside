/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.statistics

class SurvivalArrays4PetoPrentice(
                      observationTimes: Array[java.lang.Double]
                    ) extends SurvivalArrays(
                        observationTimes
                      ){

  val weights: Array[Double] = new Array[Double](nTimepoints)
  val denominatorConst: Array[Double] = new Array[Double](nTimepoints)


  weights(0) = (atRisk(0) + 1.0 - deaths(0)) / (atRisk(0) + 1.0)
  for (i <- 1 to (nTimepoints-1)){
    weights(i) = weights(i-1) * (atRisk(i) + 1 - deaths(i)) / (atRisk(i) + 1)
  }

  for (i <- 0 to (nTimepoints-1)){
    if (atRisk(i)>1){
      denominatorConst(i) =  (weights(i) * weights(i) * deaths(i) * (atRisk(i) - deaths(i))) / (atRisk(i)*atRisk(i)*(atRisk(i)-1))
    }
  }
}
