/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines

import broadside.engines.scoring.{JSATTopDownScoring, JSATTreeWalker}
import jsat.classifiers.trees.{DecisionTree, TreeLearner}
import jsat.regression.Regressor

import scala.collection.Seq

/**
  * Created by luk on 2/26/17.
  */
class BroadsideRegressionTrees extends BroadsideRegressionCore with JSATTopDownScoring with JSATTreeWalker{
  var minObsInLeaf = config.getInt("numerical.trees.min_obs_in_leaf")

  override def setParameter(key: String, value: String): String = {
    import broadside.utils.Checkers._
    var status="SUCCESS"
    key match{
      case "min_obs_in_leaf" => {
        status = isInteger(value,lower=1)
        if (ok(status)) minObsInLeaf = value.toInt
      }
      case _ => status = super.setParameter(key,value)
    }

    status
  }

  override def getParameterInfo = Seq.concat(
    super.getParameterInfo,
    Seq(
      Array("min_obs_in_leaf",minObsInLeaf.toString,"positive integer","miminal number of observations in each node of decision tree")
    )
  )

  override protected def fillParameters(child: Broadside): Unit = {
    super.fillParameters(child)
    child.asInstanceOf[BroadsideRegressionTrees].minObsInLeaf = this.minObsInLeaf
  }

  override def getInstance: Broadside = {
    val instance = new BroadsideRegressionTrees
    fillParameters(instance)
    instance
  }

  override protected def getModel: Regressor = {
    val tree = DecisionTree.getC45Tree
    tree.setMinResultSplitSize(minObsInLeaf)
    tree
  }

  override protected def getFeatureHints(model: Object, nReal: Int): (Option[(java.util.List[Int],java.util.List[Int])]) = {
    val stree = model.asInstanceOf[TreeLearner].getTreeNodeVisitor

    val featuresCat = new java.util.LinkedList[Int]()
    val featuresReal = new java.util.LinkedList[Int]()


    findFeaturesUsed(
      stree,
      featuresCat,
      featuresReal,
      nReal
    )

    Some((featuresCat,featuresReal))
  }


}
