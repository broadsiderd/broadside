/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines

import java.util

import broadside.control.Worker
import broadside.data.{DataType, Dataset, Feature, PartialStatus}
import com.typesafe.config.{Config, ConfigFactory}
import broadside.engines.scoring.JSATScoring
import broadside.utils.Samplers
import jsat.classifiers.{ClassificationDataSet, Classifier, DataPoint}

import scala.collection.mutable
import scala.collection.mutable.{ArraySeq, HashMap}



/**
 * Created by luk on 10/11/15.
 */
abstract class Broadside extends java.io.Serializable with JSATScoring {

  protected var workerContext: Option[Worker] = None
  val config = ConfigFactory.load("defaults").getConfig("dmcfs.defaults")


  var interactions = config.getBoolean("interactions")
  var s_initial = config.getInt("s_initial")
  var s_delta = config.getInt("s_delta")
  var s_max = config.getInt("s_max")
  var s_min = config.getInt("s_min")
  var m_absolute = config.getInt("m_absolute")
  var m_fraction = config.getDouble("m_fraction")
  var t = config.getInt("t")
  var tt_ratio = config.getDouble("tt_ratio")
  var p = config.getInt("p")
  var p_interactions = config.getInt("p_interactions")
  var feature_list_limit = config.getInt("feature_list_limit")
  var perm = config.getInt("perm")
  var perm_scoring = config.getInt("perm_scoring")
  var perm_scoring_alpha = config.getDouble("perm_scoring_alpha")



  var dataset: Option[Dataset] = None


  var response: Feature = null
  var featureNames: Array[String] = null
  var featureSubset: Seq[Feature] = null
  var featuresReal: Seq[Feature] = null
  var featuresCat: Seq[Feature] = null
  var featureNamesReal: Array[String] = null
  var featureNamesCat: Array[String] = null
  var partialStatus: PartialStatus = null
  var preprocessingDone: Boolean = false



  var sample: ArraySeq[DataPoint] = null
  var classCount = 0


  def setWorkerContext(wc: Worker): Unit = {
    workerContext = Some(wc)
  }

  protected def getScoringPermutations = perm_scoring
  protected def getScoringPermutationsAlpha = perm_scoring_alpha





  private def unknownParameterInfo(key: String) = "ERROR: Parameter " + key + " not recognized for given feature selector."

  protected def ok(msg: String) = {
    if (msg.contains("SUCCESS")) true
    else false
  }


  def setParameter(key: String, value: String): String = {


    import broadside.utils.Checkers._
    var status = "SUCCESS"
    key match{
      case "interactions"=> {
        status = isBoolean(value)

        if (ok(status)){
          interactions=value.toBoolean
        }
      }

      case "s_initial"=>{
        status = isInteger(value,lower=1)
        if ((value.toInt==0) || (value.toInt % s_delta)!=0){
          status = "ERROR: s_initial must be a multiple of s_delta."
        }

        if (ok(status)){
          s_initial = value.toInt
        }
      }
      case "s_delta"=>{
        status = isInteger(value,lower=1)
        if (ok(status)){
          s_delta = value.toInt

          if ( (s_initial==0) || (s_initial % s_delta)!=0){
            s_initial = s_delta
          }

        }
      }
      case "s_max"=>{
        status = isInteger(value,lower=1)
        if (ok(status)) s_max = value.toInt
      }
      case "s_min"=>{
        status = isInteger(value,lower=1)
        if (ok(status)) s_min = value.toInt
      }
      case "m_absolute"=>{
        status = isInteger(value,lower=1)
        if (ok(status)) m_absolute = value.toInt
      }
      case "m_fraction"=>{
        status = isFraction(value,lowerOpen=false,upperOpen=false)
        if (ok(status)) m_fraction = value.toDouble
      }
      case "t"=>{
        status = isInteger(value,lower=1)
        if (ok(status)) t = value.toInt
      }
      case "tt_ratio"=>{
        status = isFraction(value,lowerOpen=true,upperOpen=true)
        if (ok(status)) tt_ratio = value.toDouble
      }

      case "p" =>{
        status = isInteger(value,lower=0)
        if (ok(status)) p = value.toInt
      }

      case "p_interactions" =>{
        status = isInteger(value,lower=0)
        if (ok(status)) p_interactions = value.toInt
      }

      case "feature_list_limit" => {
        status = isInteger(value,lower=0)
        if (ok(status)) feature_list_limit = value.toInt
      }
      case "perm"=>{
        status = isInteger(value,lower=0)
        if (ok(status)) perm = value.toInt
      }
      case "perm_scoring"=>{
        status = isInteger(value,lower=1)
        if (ok(status)) perm_scoring = value.toInt
      }
      case _ => status = unknownParameterInfo(key)
    }


    status
  }


  def getParameterInfo = Seq(
      Array("interactions",interactions.toString,"boolean","Controls whether interaction mining is to be performed."),
      Array("s_initial",s_initial.toString,"positive integer","Number of feature samples against which first ranking is calculated."),
      Array("s_delta",s_delta.toString,"positive integer","Number of samples increment in subsequent iterations."),
      Array("s_max",s_max.toString,"positive integer","The number of samples at which to stop."),
      Array("s_min",s_max.toString,"positive integer","Minimal number of iterations to be completed."),
      Array("m_absolute",m_absolute.toString,"positive integer","Number of features to be included in a sample."),
      Array("m_fraction",m_fraction.toString,"[0,1] real","Fraction of features to be used in a sample, overrides m_absolute unless set to 0."),
      Array("t",t.toString,"positive integer","Number of train/test splits to be created for each sample."),
      Array("tt_ratio",t.toString,"(0,1) real","Fraction of train observations in a split."),
      Array("p",p.toString,"positive integer","Number of features in upper part of feature ranking to be included in calculating the ranking distance."),
      Array("p_interactions",p.toString,"positive integer","Number of interactions in upper part of interaction ranking to be included in calculating the ranking distance."),
      Array("feature_list_limit",feature_list_limit.toString,"positive integer","Number of best features to include in the output. (0 - all)."),
      Array("perm",perm.toString,"positive integer", "Number of permutation tests to be performed after reaching stable feature ranking."),
      Array("perm_scoring",perm_scoring.toString,"positive integer","Number of permutation repeats for Feature and Interaction scoring permutations.")
    )


  protected def fillParameters(child: Broadside): Unit = {
    child.interactions = this.interactions
    child.s_initial = this.s_initial
    child.s_delta = this.s_delta
    child.s_min = this.s_min
    child.s_max = this.s_max
    child.m_absolute = this.m_absolute
    child.m_fraction = this.m_fraction
    child.t = this.t
    child.tt_ratio = this.tt_ratio
    child.p = this.p
    child.p_interactions = this.p_interactions
    child.feature_list_limit = this.feature_list_limit
    child.perm = this.perm
    child.perm_scoring = this.perm_scoring
    child.dataset = Some(this.dataset.get.copy)
  }

  def getInstance: Broadside = null

  def isReady: String = {
    if (dataset.isEmpty)
      "INFO: FALSE Dataset not assigned."
    else
      "INFO: TRUE"
  }



  def preprocess: PartialStatus = {
    try {

      response = workerContext.get.getDecision
      featureNames = workerContext.get.getFeatureNames


      preprocessingDone = true
      PartialStatus.technical("Preprocessing succesfull.")
    }
    catch {
      case e: Exception => {
        e.printStackTrace()
        PartialStatus.error("Error during preprocessing.")
      }
    }

  }


  def iterate: PartialStatus = {
    var timeStart = System.nanoTime()

    partialStatus = PartialStatus.standard
    for (featureSample <- 1 to s_delta){
      getSample
      for (caseSample <-1 to t){
        trainAndScore
      }
    }

    partialStatus.timeTotal = System.nanoTime() - timeStart
    partialStatus
  }

  def permute: PartialStatus = {
    if (!preprocessingDone) preprocess
    val originalDecision = this.response.copy()
    this.response = this.response.copy(values = Samplers.permutation(response.values).toArray)
    val status = iterate
    this.response = originalDecision

    status.permutations = true
    status
  }

  protected def getSample = {
    //narrow down the features
    featureSubset = workerContext.get.getSample





    //separate real and categorical features
    featuresReal = featureSubset.filter(v => v.featureType==DataType.Real)
    featuresCat = featureSubset.filter(v => v.featureType==DataType.Nominal)

    //preserve the names of real and categorical features
    featureNamesReal = new Array[String](featuresReal.length)
    featureNamesCat = new Array[String](featuresCat.length)



    for (i <- 1 to featureNamesReal.length){
      featureNamesReal(i-1) = featuresReal(i-1).name
    }

    for (i <- 1 to featureNamesCat.length){
      featureNamesCat(i-1) = featuresCat(i-1).name
    }



    for (feat <- featureNamesReal.union(featureNamesCat)){
      partialStatus.partialFeatureProjections += ((feat,partialStatus.partialFeatureProjections.get(feat).getOrElse(0)+t))
    }

    getSampleSpecifics
  }

  protected def getSampleSpecifics

  protected def trainAndScore: Unit = {

    //if (featureNamesReal.contains("X1") && featureNamesReal.contains("X2")){
    //  val x = 1
    //}

    var success=false
    var totalEffects: HashMap[String,Double] = null
    var mainEffects: HashMap[String,Double] = null
    var interactionEffects: HashMap[(String,String),Double] = null

    while(!success){
      try{
        val split = ttSplit
        var timeStart = System.nanoTime()
        val model = trainSpecifics(split._1,split._3)
        partialStatus.timeTrain+= (System.nanoTime() - timeStart)
        totalEffects = new HashMap[String,Double]()
        mainEffects = new HashMap[String,Double]()
        if (interactions)
          interactionEffects = new HashMap[(String,String),Double]()

          getFeatureMetrics(
            model,
            featureNamesReal,
            featureNamesCat,
            split._1,
            split._2,
            split._3,
            split._4,
            totalEffects,
            mainEffects,
            interactionEffects,
            partialStatus,
            getFeatureHints(model,featureNamesReal.length)
          )






        success=true
      }
      catch{
        case e: Exception => {
          //System.out.println(System.nanoTime())
          e.printStackTrace()
        }
      }
    }

    for (feat <- totalEffects) {
      partialStatus.partialTotalEffects += ((feat._1,partialStatus.partialTotalEffects.get(feat._1).getOrElse(0.0) + feat._2))
    }

    for (feat <- mainEffects) {
      partialStatus.partialMainEffects += ((feat._1,partialStatus.partialMainEffects.get(feat._1).getOrElse(0.0) + feat._2))
    }

    if (interactions)
    for (id <- interactionEffects){
      partialStatus.partialInteractionEffects += (((id._1._1,id._1._2),partialStatus.partialInteractionEffects.get((id._1._1,id._1._2)).getOrElse(0.0) + id._2))
    }

  }

  protected def trainSpecifics(
                                      trainingSet : java.util.ArrayList[DataPoint],
                                      trainingDecision: java.util.ArrayList[java.lang.Double] = null
                                      ) : Object

  protected def ttSplit: (java.util.ArrayList[DataPoint],java.util.ArrayList[DataPoint],java.util.ArrayList[java.lang.Double],java.util.ArrayList[java.lang.Double])

  protected def getModel: Object

  protected def getFeatureHints(model: Object, nCat: Int): (Option[(java.util.List[Int],java.util.List[Int])]) = None



}

object Broadside{
  val generator = Map(
    "MCFS" -> Some(new BroadsideMCFS),
    "CLASSIFICATIONTREES" -> Some(new BroadsideClassificationTrees),
    "REGRESSIONTREES" -> Some(new BroadsideRegressionTrees),
    "SURVIVALTREES" -> Some(new BroadsideSurvivalTrees),

    "RANDOMFORESTS" -> Some(new BroadsideRandomForest),
    "EXTRATREES" -> Some(new BroadsideExtraTrees)

  ).withDefaultValue(None)

  def getValidNames = Seq(
    Array("MCFS","Classical MCFS."),
    Array("CLASSIFICATIONTREES","Classification Trees."),
    Array("REGRESSIONTREES","Regression Trees"),
    Array("SURVIVALTREES","Survival Trees"),

    Array("RANDOMFORESTS","MCFS framework utilising a RandomForests classifier and permutation based metrics."),
    Array("EXTRATREES","MCFS framework utilising Extra Randomized Decision Trees and permutation based metrics.")
  )

  def apply(name: String): Option[Broadside] = generator(name)



}