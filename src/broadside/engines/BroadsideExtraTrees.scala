/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines
import broadside.engines.scoring.JSATTopDownScoring
import jsat.classifiers.trees.ERTrees
import jsat.classifiers.trees.ImpurityScore.ImpurityMeasure

/**
  * Created by luk on 8/29/16.
  */
class BroadsideExtraTrees extends BroadsideTreeEnsemble with JSATTopDownScoring {

  override protected def getModel: Object = {
    val model = new ERTrees
    model.setBinaryCategoricalSplitting(true)
    model.setImpurityMeasure(ImpurityMeasure.INFORMATION_GAIN_RATIO)
    model.setSelectionCount(Integer.MAX_VALUE)
    model.setForrestSize(maxTrees)
    model.setUseDefaultStopSize(true)

    model
  }

  override def getInstance: Broadside = {
    val instance = new BroadsideExtraTrees
    fillParameters(instance)
    instance
  }
}
