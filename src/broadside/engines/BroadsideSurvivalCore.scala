/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines

import broadside.engines.scoring.metrics.JSATConcordanceIndex
import broadside.engines.scoring.metrics.JSATMeanAbsoluteError


/**
  * Created by luk on 3/11/17.
  */
abstract class BroadsideSurvivalCore extends BroadsideNumericalCore with  JSATConcordanceIndex {

  c_index_factor =  config.getDouble("survival.c_index_multiplier")
}
