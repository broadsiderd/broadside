/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines.scoring

import java.util
import java.util.List

import broadside.utils.Samplers
import jsat.classifiers.{Classifier, DataPoint}

import scala.collection.mutable
import scala.collection.mutable.HashMap

/**
  * Created by luk on 8/12/16.
  */
trait JSATPermuter {




  protected def getScoringPermutations: Int
  protected def getScoringPermutationsAlpha: Double


  protected def backupNumeric(data: List[DataPoint], index: Int): Array[Double] = {
    val buffer = new Array[Double](data.size())
    for (i <- 1 to buffer.size) {
      buffer(i - 1) = data.get(i - 1).getNumericalValues.get(index)
    }
    buffer
  }


  protected def permuteNumeric(data: List[DataPoint], index: Int): Array[Double] = {
    val numericBuffer = backupNumeric(data,index)
    val sequence = Samplers.permutation((0 to (numericBuffer.length-1)).toArray)

    for (i <- 1 to numericBuffer.size){
      data.get(i-1).getNumericalValues.set(index,numericBuffer(sequence(i-1)))
    }

    numericBuffer
  }

  protected def restoreNumeric(data: List[DataPoint], index: Int, numericBuffer: Array[Double]) = {
    for (i <-1 to numericBuffer.size){
      data.get(i-1).getNumericalValues.set(index,numericBuffer(i-1))
    }
  }

  protected def backupCategorical(data: List[DataPoint], index: Int): Array[Int] = {
    val buffer = new Array[Int](data.size())
    for (i <- 1 to buffer.size){
      buffer(i-1) = data.get(i - 1).getCategoricalValue(index)
    }
    buffer
  }


  protected def permuteCategorical(data: List[DataPoint], index: Int, shift: Int = 1): Array[Int] = {
    val categoricalBuffer = backupCategorical(data,index-1+shift)
    val sequence = Samplers.permutation((0 to (categoricalBuffer.length-1)).toArray)

    for (i <- 1 to categoricalBuffer.size){
      (data.get(i-1).getCategoricalValues)(index-1+shift) = categoricalBuffer(sequence(i-1))
    }

    categoricalBuffer
  }

  protected def restoreCategorical(data: List[DataPoint], index: Int, categoricalBuffer: Array[Int], shift: Int = 1) = {
    for (i <-1 to categoricalBuffer.size){
      (data.get(i-1).getCategoricalValues)(index-1+shift) = categoricalBuffer(i-1)
    }
  }




  protected def groupByClass(data: List[DataPoint]): mutable.ListMap[Int,List[DataPoint]] = {
    val permuted = new mutable.ListMap[Int,List[DataPoint]]
    for (i <- 1 to data.size()){
      val cat = data.get(i-1).getCategoricalValue(0)
      if (!permuted.contains(cat)) permuted +=  ((cat,new util.LinkedList[DataPoint]))
      permuted.get(cat).get.add(data.get(i-1))
    }
    permuted
  }

}
