/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines.scoring

import java.util


import jsat.classifiers.{Classifier, DataPoint}
import broadside.data.PartialStatus

import scala.collection.mutable

/**
  * Created by luk on 8/29/16.
  */
trait JSATScoring {

  protected def assessModel(model: Object, testSet: java.util.List[DataPoint], testDecision: java.util.List[java.lang.Double], classCount: Int , inverse: Boolean = true): Double

  protected def getFeatureMetrics(model: Object,
                                  featureNamesReal: Array[String],
                                  featureNamesCategorical: Array[String],
                                  trainPoints: util.List[DataPoint],
                                  testPoints: util.List[DataPoint],
                                  trainDecision: util.List[java.lang.Double] = null,
                                  testDecision: util.List[java.lang.Double] = null,
                                  totalEffects: mutable.HashMap[String, Double],
                                  mainEffects: mutable.HashMap[String, Double],
                                  interactionEffects: mutable.HashMap[(String, String), Double],
                                  partialStatus: PartialStatus,
                                  featureHints: Option[(java.util.List[Int],java.util.List[Int])]
                                 ): Unit

  protected def getNumberOfClasses: Int = 0

}
