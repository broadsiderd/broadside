/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines.scoring

import jsat.classifiers.trees.{DecisionTree, TreeLearner, TreeNodeVisitor}
import java.util.List

trait JSATTreeWalker {

  protected def findFeaturesUsed(
                                  walker: TreeNodeVisitor,
                                  categorical: java.util.List[Int],
                                  numeric: java.util.List[Int],
                                  nreal: Int

                                ): Unit = {

    if (walker!=null &&  walker.childrenCount()>1) {
      val fused = walker.featuresUsed().iterator().next()

      if (fused >= nreal) {
        if (!categorical.contains(fused-nreal+1))
          categorical.add(fused-nreal+1)
      }
      else {
        if (!numeric.contains(fused+1))
          numeric.add(fused+1)
      }


      for (i <- 1 to walker.childrenCount()){
        findFeaturesUsed(
          walker.getChild(i-1),
          categorical,
          numeric,
          nreal
        )
      }

    }
  }

}
