/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines.scoring.metrics

import java.util

import broadside.engines.scoring.JSATScoring
import jsat.classifiers.DataPoint
import jsat.regression.Regressor

/**
  * Created by luk on 2/26/17.
  */
trait JSATMeanAbsoluteError extends JSATScoring{

  override protected def assessModel(model: Object, testSet: util.List[DataPoint], testDecision: util.List[java.lang.Double], classCount: Int, inverrse: Boolean = true): Double = {
    var mea = 0.0
    val regressor = model.asInstanceOf[Regressor]

    for (i <- 1 to testSet.size){
      mea += math.abs(regressor.regress(testSet.get(i-1)) - testDecision.get(i-1))
    }

    mea = mea / testSet.size
    mea
  }
}
