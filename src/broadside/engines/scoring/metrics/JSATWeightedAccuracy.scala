/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines.scoring.metrics

import broadside.engines.scoring.JSATScoring
import jsat.classifiers.{Classifier, DataPoint}

/**
  * Created by luk on 8/28/16.
  */
trait JSATWeightedAccuracy extends JSATScoring {

  override protected def assessModel(model: Object, testSet: java.util.List[DataPoint], trainSet: java.util.List[java.lang.Double], classCount: Int, inverse: Boolean = true): Double = {
    val accHits = new Array[Int](classCount)
    val classSizes = new Array[Int](classCount)
    val classifier = model.asInstanceOf[Classifier]

    for (dpIdx <- 1 to testSet.size){
      val dp = testSet.get(dpIdx-1)
      val trueClass = dp.getCategoricalValue(0)
      classSizes(trueClass)+=1

      val categoricalValues = new Array[Int](dp.getCategoricalValues.length-1)
      System.arraycopy(dp.getCategoricalValues,1,categoricalValues,0,categoricalValues.length)
      val predictedClass = classifier.classify(new DataPoint(dp.getNumericalValues,categoricalValues,null)).mostLikely()

      if (trueClass==predictedClass)
        accHits(trueClass)+=1

    }

    val wAcc = classSizes.zip(accHits).map(v => ((v._2.toDouble / v._1.toDouble)/classSizes.length )).sum

    if (inverse) 1 - wAcc else wAcc
  }

}
