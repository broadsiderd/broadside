/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines.scoring.metrics

import java.util

import broadside.engines.scoring.JSATScoring
import broadside.utils.Samplers
import jsat.classifiers.DataPoint
import jsat.regression.Regressor
import scala.util.control.Breaks._

/**
  * Created by luk on 3/11/17.
  */
trait JSATConcordanceIndex extends JSATScoring{
  var c_index_factor = 1.0

  override protected def assessModel(model: Object, testSet: util.List[DataPoint], testDecision: util.List[java.lang.Double], classCount: Int, inverse: Boolean = true): Double = {

    val survModel = model.asInstanceOf[Regressor]

    val nTest = testDecision.size
    if (nTest<2) return 1.0
    var concordance = 0.0
    var permissible_target = math.ceil(testDecision.size()*c_index_factor)
    var nBreak = 0

    var permissible = 0.0


    while ((permissible < permissible_target) && nBreak<100  ){
      val idxFirst = Samplers.singleNum(nTest)
      val idxSecond = Samplers.singleNum(nTest)

      if (idxFirst == idxSecond) {
        nBreak += 1
      }
      else{


        val timeFirst = testDecision.get(idxFirst-1)
        val timeSecond = testDecision.get(idxSecond-1)

        var pointTuple = if (math.abs(testDecision.get(idxFirst-1))<math.abs(testDecision.get(idxSecond-1))) (idxFirst,idxSecond) else (idxSecond,idxFirst)

        if (( testDecision.get(pointTuple._1-1) < 0) && (timeFirst == -timeSecond) )
          pointTuple = (pointTuple._2,pointTuple._1)

        if (testDecision.get(pointTuple._1-1) < 0){
          nBreak += 1
        }
        else {

          nBreak = 0
          permissible += 1



          val pred1 = survModel.regress(testSet.get(pointTuple._1 - 1))
          val pred2 = survModel.regress(testSet.get(pointTuple._2 - 1))

          if (timeFirst != timeSecond){

            if (pred1 > pred2)
              concordance += 1.0
            else if (pred1 == pred2)
              concordance += 0.5

          } else if ( testDecision.get(pointTuple._2-1)  > 0){
            if (pred1 == pred2)
              concordance += 1.0
            else
              concordance += 0.5
          } else {

            if (pred1 > pred2)
              concordance += 1.0
            else if (pred1 == pred2)
              concordance += 0.5

          }

        }

      }




    }

    /*
    val regressor = model.asInstanceOf[Regressor]

    for (i <- 1 to testSet.size){
      mea += math.abs(regressor.regress(testSet.get(i-1)) - testDecision.get(i-1))
    }

    mea = mea / testSet.size
    mea
    */


    var c_index = if (nBreak==100) 0.0 else concordance.toDouble / permissible.toDouble

    if (inverse)
      c_index = 1.0 - c_index

    c_index
/*
    val regressor = model.asInstanceOf[Regressor]
    var mea: Double = 0.0
    for (i <- 1 to testSet.size){
      mea += math.abs(regressor.regress(testSet.get(i-1)) - testDecision.get(i-1))
    }

    mea = mea / testSet.size
    mea
    */
  }
}
