/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines.scoring

import java.util

import Jama.Matrix
import broadside.utils.Samplers
import jsat.classifiers.{Classifier, DataPoint}
import jsat.linear.DenseVector
import jsat.testing.onesample.TTest
import jsat.testing.StatisticTest.H1._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by luk on 8/12/16.
  */
trait JSATTopDownScoring extends JSATScoring with JSATPermuter {
  import java.util.List



  override protected def getFeatureMetrics(model: Object,
                                           featureNamesReal: Array[String],
                                           featureNamesCategorical: Array[String],
                                           trainPoints: util.List[DataPoint],
                                           testPoints: util.List[DataPoint],
                                           trainDecision: util.List[java.lang.Double] = null,
                                           testDecision: util.List[java.lang.Double] = null,
                                           totalEffects: mutable.HashMap[String,Double],
                                           mainEffects: mutable.HashMap[String, Double],
                                           interactionEffects: mutable.HashMap[(String, String), Double],
                                           partialStatus: broadside.data.PartialStatus,
                                           featureHints: Option[(java.util.List[Int],java.util.List[Int])]
                                 ): Unit = {


    var realToScan: util.List[Int] = null
    var categoricalToScan: util.List[Int] = null

    if (featureHints.isEmpty){
      realToScan = new util.LinkedList[Int]()
      for (i <- 1 to featureNamesReal.length){
        realToScan.add(i)
      }
      categoricalToScan = new util.LinkedList[Int]()
      for (i <- 1 to featureNamesCategorical.length){
        categoricalToScan.add(i)
      }
    }
    else {
      realToScan = featureHints.get._2
      categoricalToScan = featureHints.get._1
    }





    var timeStart = System.nanoTime()
    //FAIL!!!
    val shift = if (testDecision==null) 1 else 0





    val wAccUnpermuted = assessModel(model, testPoints, testDecision, getNumberOfClasses)
    val usefulReal = new mutable.HashSet[Int]()
    val usefulCategorical = new mutable.HashSet[Int]()
    val usefulRealTotalEffects = new ArrayBuffer[Double]()
    val usefulCategoricalTotalEffects = new ArrayBuffer[Double]()

    var realTotalEffect = 0.0
    val realToScanIterator = realToScan.iterator()

    while (realToScanIterator.hasNext) {
      val i = realToScanIterator.next()
      val wAccDeltas = new Array[Double](getScoringPermutations)
      for (j <- 1 to getScoringPermutations) {
        val buffer = permuteNumeric(testPoints, i - 1)
        wAccDeltas(j - 1) = assessModel(model, testPoints, testDecision, getNumberOfClasses)- wAccUnpermuted
          restoreNumeric(testPoints, i - 1, buffer)
      }


      val test = new TTest(GREATER_THAN, 0.0, new DenseVector(wAccDeltas))

      if (test.pValue() < getScoringPermutationsAlpha) {
        usefulReal += i
        realTotalEffect = Samplers.avg(wAccDeltas)
        usefulRealTotalEffects += (realTotalEffect)
        totalEffects += ((featureNamesReal(i-1), totalEffects.get( featureNamesReal(i-1)).getOrElse(0.0) + realTotalEffect  ))
      }

    }

    var categoricalTotalEffect = 0.0
    val categoricalToScanIterator = categoricalToScan.iterator()

    while (categoricalToScanIterator.hasNext) {
      val i = categoricalToScanIterator.next()
      val wAccDeltas = new Array[Double](getScoringPermutations)
      for (j <- 1 to getScoringPermutations) {
        val buffer = permuteCategorical(testPoints, i, shift)
        wAccDeltas(j - 1) = assessModel(model, testPoints, testDecision, getNumberOfClasses) - wAccUnpermuted
        restoreCategorical(testPoints, i, buffer, shift)
      }


      val test = new TTest(GREATER_THAN, 0.0, new DenseVector(wAccDeltas))

      if (test.pValue() < getScoringPermutationsAlpha) {
        usefulCategorical += i
        categoricalTotalEffect = Samplers.avg(wAccDeltas)
        usefulCategoricalTotalEffects += (categoricalTotalEffect)
        totalEffects += ((featureNamesCategorical(i-1),totalEffects.get(featureNamesCategorical(i-1)).getOrElse(0.0) + categoricalTotalEffect))
      }

    }


    partialStatus.timeScoreFeatures += (System.nanoTime() - timeStart)

    timeStart = System.nanoTime()




    if (((usefulReal.size + usefulCategorical.size) == 1) && interactionEffects!=null){
      if (usefulReal.size==1){
        val featIdx = usefulReal.toList.last
        val featName = featureNamesReal(featIdx - 1)
        mainEffects += ((featName,mainEffects.get(featName).getOrElse(0.0)+realTotalEffect))
      }
      else{
        val featIdx = usefulCategorical.toList.last
        val featName = featureNamesCategorical(featIdx - 1)
        mainEffects += ((featName,mainEffects.get(featName).getOrElse(0.0)+categoricalTotalEffect))
      }
    }
    else if (((usefulReal.size + usefulCategorical.size) > 1) && interactionEffects!=null) {





      val featuresSingle = new ArrayBuffer[Int](usefulCategorical.size + usefulReal.size)
      if (usefulCategorical.size > 0) usefulCategorical.toSeq.sortWith((v1, v2) => (v1 < v2)).map(v => featuresSingle += -v)
      if (usefulReal.size > 0) usefulReal.toSeq.sortWith((v1, v2) => (v1 < v2)).map(v => featuresSingle += v)
      val featuresPairs = Samplers.combinations(featuresSingle)

      val lhs = new Array[Array[Double]](featuresSingle.size + featuresPairs.size)
      val rhs = new Array[Double](featuresSingle.size + featuresPairs.size)

      for (i <- 1 to featuresSingle.length) {
        val feature = featuresSingle(i - 1)
        val leftFeatures = featuresSingle.map(v => if (v == feature) 1.0 else 0.0).toArray
        val leftPairs = featuresPairs.map(v => if (v._1 == feature || v._2 == feature) 1.0 else 0.0).toArray
        lhs(i - 1) = leftFeatures ++ leftPairs
      }


        System.arraycopy(usefulCategoricalTotalEffects.toArray, 0, rhs, 0, usefulCategoricalTotalEffects.length)
        System.arraycopy(usefulRealTotalEffects.toArray, 0, rhs, usefulCategoricalTotalEffects.length, usefulRealTotalEffects.length)

      for (i <- 1 to featuresPairs.length) {
        val pair = featuresPairs(i - 1)
        val leftFeatures = featuresSingle.map(v => if ((v == pair._1) || (v == pair._2)) 1.0 else 0.0).toArray
        val leftPairs = featuresPairs.map(v => if (v._1 == pair._1 || v._2 == pair._1 || v._1 == pair._2 || v._2 == pair._2) 1.0 else 0.0).toArray
        lhs(i - 1 + featuresSingle.length) = leftFeatures ++ leftPairs

        val wAccDeltas = new Array[Double](getScoringPermutations)
        for (j <- 1 to getScoringPermutations) {
          val buffer1 = if (pair._1 < 0) permuteCategorical(testPoints, -pair._1, shift) else permuteNumeric(testPoints, pair._1-1)
          val buffer2 = if (pair._2 < 0) permuteCategorical(testPoints, -pair._2, shift) else permuteNumeric(testPoints, pair._2-1)
          wAccDeltas(j - 1) = assessModel(model, testPoints, testDecision, getNumberOfClasses) - wAccUnpermuted

          if (pair._1 < 0) restoreCategorical(testPoints, -pair._1, buffer1.asInstanceOf[Array[Int]], shift) else restoreNumeric(testPoints, pair._1-1, buffer1.asInstanceOf[Array[Double]])
          if (pair._2 < 0) restoreCategorical(testPoints, -pair._2, buffer2.asInstanceOf[Array[Int]], shift) else restoreNumeric(testPoints, pair._2-1, buffer2.asInstanceOf[Array[Double]])
        }

        val test = new TTest(GREATER_THAN, 0.0, new DenseVector(wAccDeltas))
        if (test.pValue() < getScoringPermutationsAlpha)
          rhs(i - 1 + featuresSingle.length) = Samplers.avg(wAccDeltas)
        else
          rhs(i - 1 + featuresSingle.length) = 0.0

      }

      val lhsMatrix = new Matrix(lhs)
      val rhsMatrix = new Matrix(rhs, rhs.length)
      val solution = lhsMatrix.solve(rhsMatrix)

      for (i <-1 to featuresSingle.length){
        val featureName = if (featuresSingle(i-1)<0) featureNamesCategorical(-featuresSingle(i-1)-1)
        else featureNamesReal(featuresSingle(i-1)-1)

        val featureMainEffect = solution.get(i-1,0)

        mainEffects += ((featureName,mainEffects.get(featureName).getOrElse(0.0) + featureMainEffect))

      }




      for (i <-1 to featuresPairs.length){
        val featureName1 = if (featuresPairs(i-1)._1<0) featureNamesCategorical(-featuresPairs(i-1)._1-1)
        else featureNamesReal(featuresPairs(i-1)._1-1)

        val featureName2 = if (featuresPairs(i-1)._2<0) featureNamesCategorical(-featuresPairs(i-1)._2-1)
        else featureNamesReal(featuresPairs(i-1)._2-1)


        val interactionEffect = solution.get(i-1 + featuresSingle.length,0)

        if (interactionEffect>0 ){
          val interactionAlphabetical = if (featureName1<=featureName2) (featureName1,featureName2) else (featureName2,featureName1)
          interactionEffects += (( interactionAlphabetical , interactionEffects.get(interactionAlphabetical).getOrElse(0.0) + interactionEffect ))
        }


      }
    }

    partialStatus.timeScoreInteractions += (System.nanoTime() - timeStart)
  }


}
