/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines

import java.util

import broadside.engines.scoring.{JSATTopDownScoring, JSATTreeWalker}
import broadside.jsatmods.survival.DecisionTree
import jsat.classifiers.trees.TreeLearner
//import jsat.classifiers.trees.DecisionTree
import jsat.regression.Regressor

import scala.collection.Seq

/**
  * Created by luk on 3/12/17.
  */
class BroadsideSurvivalTrees extends BroadsideSurvivalCore with JSATTopDownScoring with JSATTreeWalker {
  var minDeath = config.getInt("survival.trees.min_obs_in_leaf")

  override def setParameter(key: String, value: String): String = {
    import broadside.utils.Checkers._
    var status="SUCCESS"
    key match{
      case "min_obs_in_leaf" => {
        status = isInteger(value,lower=1)
        if (ok(status)) minDeath = value.toInt
      }
      case _ => status = super.setParameter(key,value)
    }

    status
  }

  override def getParameterInfo = Seq.concat(
    super.getParameterInfo,
    Seq(
      Array("min_obs_in_leaf",minDeath.toString,"positive integer","miminal number of observations in each node of decision tree")
    )
  )

  override protected def fillParameters(child: Broadside): Unit = {
    super.fillParameters(child)
    child.asInstanceOf[BroadsideSurvivalTrees].minDeath = this.minDeath
  }

  override def getInstance: Broadside = {
    val instance = new BroadsideSurvivalTrees
    fillParameters(instance)
    instance
  }

  override protected def getModel: Regressor = {
    val tree = DecisionTree.getC45Tree
    tree.setMinResultSplitSize(minDeath)
    tree
  }

  override protected def getFeatureHints(model: Object, nReal: Int): (Option[(java.util.List[Int],java.util.List[Int])]) = {
    val stree = model.asInstanceOf[TreeLearner].getTreeNodeVisitor

    val featuresCat = new java.util.LinkedList[Int]()
    val featuresReal = new java.util.LinkedList[Int]()


    findFeaturesUsed(
      stree,
      featuresCat,
      featuresReal,
      nReal
    )

    Some((featuresCat,featuresReal))
  }
}
