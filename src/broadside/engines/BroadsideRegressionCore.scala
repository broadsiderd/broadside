/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines
import java.util

import broadside.engines.scoring.metrics.JSATMeanAbsoluteError
import jsat.classifiers.{CategoricalData, DataPoint, DataPointPair}
import jsat.linear.DenseVector
import jsat.regression.{RegressionDataSet, Regressor}

import scala.collection.mutable.ArraySeq


/**
  * Created by luk on 2/26/17.
  */
abstract class BroadsideRegressionCore extends BroadsideNumericalCore with JSATMeanAbsoluteError {


}
