/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines

import java.util

import broadside.engines.scoring.JSATPermuter
import broadside.engines.scoring.metrics.JSATWeightedAccuracy
import jsat.classifiers.trees.DecisionTree
import jsat.classifiers.{CategoricalData, ClassificationDataSet, Classifier, DataPoint}
import jsat.linear.DenseVector

import scala.collection.mutable.{ArraySeq, HashMap}
import scala.math._

/**
  * Created by luk on 8/11/16.
  */
abstract class BroadsideClassificationCore extends Broadside with JSATWeightedAccuracy{



  override protected def getNumberOfClasses: Int = classCount


  override protected def getSampleSpecifics: Unit = {

    //template for the categorical data in this subset, including the decision vector
    val categoricalData = new Array[CategoricalData](1+featuresCat.length)


    //set up decision category
    val decisionCategories = response.values.distinct
    categoricalData(0) = new CategoricalData(decisionCategories.length)
    categoricalData(0).setCategoryName(response.name)

    //set up other categorical/nominal input variables
    for (i <- 1 to categoricalData.length-1){
      val categories = featuresCat(i-1).values.distinct
      categoricalData(i) = new CategoricalData(categories.length)
      categoricalData(i).setCategoryName(featuresCat(i-1).name)
    }

    //prepare the datapoints
    sample = new ArraySeq[DataPoint](response.values.length)

    for (i <- 1 to response.values.length){
      val vecNumeric = new Array[Double](featuresReal.length)
      val vecCat = new Array[Int](1+featuresCat.length)

      for (j <- 1 to vecNumeric.length){
        vecNumeric(j-1) = featuresReal(j-1).values(i-1)
      }

      vecCat(0) = response.values(i-1).toInt

      for (j <-1 to vecCat.length-1){
        vecCat(j) = featuresCat(j-1).values(i-1).toInt
      }

      sample(i-1)= new DataPoint(new DenseVector(vecNumeric), vecCat, categoricalData)
    }

    classCount = decisionCategories.distinct.length

  }


  override protected def trainSpecifics(trainingPoints: util.ArrayList[DataPoint], trainingDecision: util.ArrayList[java.lang.Double]): Object = {
    val trainingSet = new ClassificationDataSet(trainingPoints,0)
    val model = getModel.asInstanceOf[Classifier]
    model.trainC(trainingSet)
    model
  }





  override protected def   ttSplit: (java.util.ArrayList[DataPoint],java.util.ArrayList[DataPoint],java.util.ArrayList[java.lang.Double],java.util.ArrayList[java.lang.Double]) = {

    val trainPoints = new java.util.ArrayList[DataPoint]
    val testPoints = new java.util.ArrayList[DataPoint]

    val annotated = sample.map(
      v => (v,v.getCategoricalValue(0),random)
    )

    val classesUnique = annotated.map(v=> v._2).distinct

    for (c <- classesUnique){
      val unsplit = annotated.filter(
        v => v._2==c
      ).map(
        v => (v._1,v._3)
      ).sortWith(
        _._2 < _._2
      ).map(
        v => v._1
      ).zipWithIndex

      val cutoff = floor(unsplit.length * tt_ratio)

      unsplit.map(
        v => {
          if ((v._2+1) <=cutoff) trainPoints.add(v._1)
          else testPoints.add(v._1)
        }
      )

    }


    (trainPoints,testPoints,null,null)
  }


}
