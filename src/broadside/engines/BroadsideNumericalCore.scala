/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines

import java.util

import jsat.classifiers.{CategoricalData, DataPoint, DataPointPair}
import jsat.linear.DenseVector
import jsat.regression.Regressor

import scala.collection.mutable.ArraySeq

/**
  * Created by luk on 3/11/17.
  */
abstract class BroadsideNumericalCore extends Broadside {
  override protected def getNumberOfClasses: Int = 0

  override protected def getSampleSpecifics: Unit = {

    //template for the categorical data in this subset, including the decision vector
    val categoricalData = new Array[CategoricalData](featuresCat.length)

    //set up other categorical/nominal input variables
    for (i <- 1 to categoricalData.length){
      val categories = featuresCat(i-1).values.distinct
      categoricalData(i-1) = new CategoricalData(categories.length)
      categoricalData(i-1).setCategoryName(featuresCat(i-1).name)
    }

    sample = new ArraySeq[DataPoint](response.values.length)

    for (i <- 1 to response.values.length){
      val vecNumeric = new Array[Double](featuresReal.length)
      val vecCat = new Array[Int](featuresCat.length)

      for (j <- 1 to vecNumeric.length){
        vecNumeric(j-1) = featuresReal(j-1).values(i-1)
      }

      for (j <-1 to vecCat.length){
        vecCat(j-1) = featuresCat(j-1).values(i-1).toInt
      }

      sample(i-1)= new DataPoint(new DenseVector(vecNumeric), vecCat, categoricalData)
    }

  }

  override protected def trainSpecifics(trainingPoints: util.ArrayList[DataPoint], trainingDecision: util.ArrayList[java.lang.Double] = null): Object = {
    val dpp = new java.util.ArrayList[DataPointPair[java.lang.Double]](trainingPoints.size)

    for (i <- 1 to trainingPoints.size){
      dpp.add(
        new DataPointPair[java.lang.Double](trainingPoints.get(i-1),trainingDecision.get(i-1))
      )
    }


    val trainingSet = new jsat.regression.RegressionDataSet(dpp)

    val model = getModel.asInstanceOf[Regressor]
    model.train(trainingSet)
    model
  }

  override protected def ttSplit: (util.ArrayList[DataPoint], util.ArrayList[DataPoint], java.util.ArrayList[java.lang.Double], java.util.ArrayList[java.lang.Double]) = {
    val trainPoints = new java.util.ArrayList[DataPoint]
    val testPoints = new java.util.ArrayList[DataPoint]
    val trainDecision = new java.util.ArrayList[java.lang.Double]
    val testDecision = new java.util.ArrayList[java.lang.Double]


    val randomized = sample.zip(response.values).map(
      v => (v,scala.math.random)
    ).sortBy(_._2).map(
      _._1
    ).zipWithIndex

    val cutoff = randomized.size * tt_ratio

    randomized.filter( _._2 + 1 <= cutoff).map( v =>
    {
      trainPoints.add(v._1._1)
      trainDecision.add(v._1._2)
    }
    )

    randomized.filter( _._2 > cutoff).map( v=>
    {
      testPoints.add(v._1._1)
      testDecision.add(v._1._2)
    }
    )

    (trainPoints,testPoints,trainDecision,testDecision)
  }
}
