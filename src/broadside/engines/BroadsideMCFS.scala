/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines

import java.util

import broadside.data.{DataType, Feature, PartialStatus}
import jsat.classifiers.{CategoricalData, ClassificationDataSet, Classifier, DataPoint}
import jsat.linear.DenseVector
import jsat.classifiers.trees.{DecisionTree, ImpurityScore, TreeNodeVisitor}

import scala.collection.{Seq, mutable}
import scala.collection.mutable.{ArrayBuffer, ArraySeq, HashMap}
import scala.math.{ceil, floor, pow, random}


/**
 * Created by luk on 10/11/15.
 */
class BroadsideMCFS extends BroadsideClassificationTrees{


  var u = config.getDouble("categorical.trees.vanilia.u")
  var v = config.getDouble("categorical.trees.vanilia.v")



  override def setParameter(key: String, value: String): String = {
    import broadside.utils.Checkers._
    var status="SUCCESS"
    key match{
      case "u" =>{
        status = isReal(value,lower=0.0)
        if (ok(status)) u = value.toDouble
      }
      case "v" =>{
        status = isReal(value,lower=0.0)
        if (ok(status)) v = value.toDouble
      }
      case _ => status = super.setParameter(key,value)
    }

    status
  }

  override def getParameterInfo = Seq.concat(
    super.getParameterInfo,
    Seq(
      Array("u",u.toString,"nonnegative real","Controls the influence of whole classifier accuracy upon feature's RI."),
      Array("v",v.toString,"nonnegative real","Controls the influence of feature's coverage upon it's RI yield. ")
    )
  )


  override protected def fillParameters(instance: Broadside): Unit ={
    super.fillParameters(instance)
    instance.asInstanceOf[BroadsideMCFS].u = u
    instance.asInstanceOf[BroadsideMCFS].v = v
  }


  override def getInstance: Broadside = {
    val instance = new BroadsideMCFS
    fillParameters(instance)
    instance
  }

  override protected def getFeatureMetrics(model: Object,
                                    featureNamesReal: Array[String],
                                    featureNamesCategorical: Array[String],
                                    trainPoints: java.util.List[DataPoint],
                                    testPoints: java.util.List[DataPoint],
                                    trainDecision: java.util.List[java.lang.Double],
                                    testDecision: java.util.List[java.lang.Double],
                                    totalEffects: mutable.HashMap[String,Double],
                                    mainEffects: mutable.HashMap[String, Double],
                                    interactionEffects: mutable.HashMap[(String, String), Double],
                                           partialStatus: broadside.data.PartialStatus,
                                           featureHints: Option[(java.util.List[Int],java.util.List[Int])]
                                 ) = {

    getTreeStructure(model.asInstanceOf[DecisionTree],trainPoints.asInstanceOf[util.ArrayList[DataPoint]],mainEffects,interactionEffects)
    val wAcc = pow(assessModel(model,testPoints.asInstanceOf[util.ArrayList[DataPoint]], testDecision, classCount,inverse = false),u)
    for (entry <- mainEffects){
      mainEffects += ((entry._1,entry._2*wAcc))
    }

    mainEffects.map(v=> totalEffects += (v))

  }





  protected def getTreeStructure(
                                  tree: DecisionTree,
                                  trainObservations: java.util.ArrayList[DataPoint],
                                  partialRIs: HashMap[String,Double],
                                  partialIDs: HashMap[(String,String),Double]

                                ): Unit = {
    val obs = new ArrayBuffer[DataPoint]
    val cats = new ArrayBuffer[Int]
    for (i <- 1 to trainObservations.size()){
      val dp = trainObservations.get(i-1)
      val catFields = new Array[Int](dp.getCategoricalValues.length-1)
      System.arraycopy(dp.getCategoricalValues,1,catFields,0,catFields.length)
      obs += new DataPoint(dp.getNumericalValues,catFields,null)
      cats += dp.getCategoricalValues()(0)
    }
    val walker = tree.getTreeNodeVisitor
    val classCount = cats.distinct.size
    _getTreeStructure(walker,obs,cats,obs.size,classCount,"",-1,partialRIs,partialIDs)
  }

  private def _getTreeStructure(
                            walker: TreeNodeVisitor,
                            datapoints: ArrayBuffer[DataPoint],
                            categories: ArrayBuffer[Int],
                            totalObservations: Int,
                            classCount: Int,
                            parentFeature: String,
                            parentObservations: Int,
                            partialRIs: HashMap[String,Double],
                            partialIDs: HashMap[(String,String),Double]
                          ): Unit = {


    if (walker!=null &&  walker.childrenCount()>1) {


        //find the feature on which the split is made
        val featureIdx = walker.featuresUsed().iterator().next()

        val featureName = if (featureIdx >= featureNamesReal.length) featureNamesCat(featureIdx - featureNamesReal.length)
        else featureNamesReal(featureIdx)


        //see how the feature increases the purity of the data
        val splits = new Array[ArrayBuffer[DataPoint]](walker.childrenCount())
        val cats = new Array[ArrayBuffer[Int]](walker.childrenCount())
        val childrenScoreCalculators = new Array[ImpurityScore](walker.childrenCount())

        for (i <- 1 to walker.childrenCount()) {
          splits(i - 1) = new ArrayBuffer[DataPoint]
          cats(i-1) = new ArrayBuffer[Int]
          childrenScoreCalculators(i - 1) = new ImpurityScore(classCount, ImpurityScore.ImpurityMeasure.INFORMATION_GAIN_RATIO)
        }

        for (i <- 1 to datapoints.size) {
          val path = walker.getPath(datapoints(i - 1))
          splits(path) += datapoints(i - 1)
          cats(path) += categories(i - 1)

          childrenScoreCalculators(path).addPoint(1.0, categories(i - 1))
        }

        val parentScoreCalculator = new ImpurityScore(classCount, ImpurityScore.ImpurityMeasure.INFORMATION_GAIN_RATIO)
        categories.map(v => parentScoreCalculator.addPoint(1.0, v))

        val parentScore = parentScoreCalculator.getScore
        var childrenScore = 0.0
        for (csc <- childrenScoreCalculators) {
          childrenScore += csc.getScore * csc.getSumOfWeights / parentScoreCalculator.getSumOfWeights
        }

        val gain = parentScore - childrenScore
        val featureRI = gain * pow(datapoints.size.toDouble / totalObservations.toDouble, v)


        if (featureRI != 0.0) {
          val totalRI = partialRIs.get(featureName).getOrElse(0.0) + featureRI
          partialRIs += ((featureName, totalRI))

          if (partialIDs != null && parentFeature != "" && parentObservations != 0) {

            val featureID = gain * (datapoints.size.toDouble / parentObservations.toDouble)
            if (featureID != 0.0) {
              val totalID = partialIDs.get((parentFeature, featureName)).getOrElse(0.0) + featureID
              partialIDs += (((parentFeature, featureName), totalID))
            }
          }

        }

        for (i <- 1 to walker.childrenCount()) {
          _getTreeStructure(walker.getChild(i - 1), splits(i - 1), cats(i - 1), totalObservations,classCount, featureName, datapoints.size, partialRIs, partialIDs)
        }

    }
  }




}

