/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.engines

import broadside.engines.scoring.JSATTopDownScoring
import jsat.classifiers.Classifier
import jsat.classifiers.trees.RandomForest

/**
  * Created by luk on 8/17/16.
  */
abstract class BroadsideTreeEnsemble extends BroadsideClassificationCore{
  var maxTrees = config.getInt("categorical.tree-ensemble.max_trees")

  override def setParameter(key: String, value: String): String = {
    import broadside.utils.Checkers._
    var status="SUCCESS"
    key match{
      case "max_trees" =>{
        status = isInteger(value,lower = 0)
        if (ok(status))  maxTrees = value.toInt
      }
      case _ => status = super.setParameter(key,value)
    }

    status
  }

  override def getParameterInfo: Seq[Array[String]] = Seq.concat(
    super.getParameterInfo,
    Seq(
      Array("max_trees",maxTrees.toString,"positive integer","Maximum number of trees in a forest.")
    )
  )

  override protected def fillParameters(child: Broadside): Unit = {
    super.fillParameters(child)
    child.asInstanceOf[BroadsideTreeEnsemble].maxTrees = maxTrees
  }


}
