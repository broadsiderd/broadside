/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.data

import broadside.api.InteractionEntry

import scala.collection.mutable.HashMap

/**
  * Created by luk on 11/27/16.
  */
class MainEffectList extends Serializable{
  var ranking = new HashMap[String,Double]



  def flush  = {
    ranking.clear
    ranking = new HashMap[String,Double]
  }

  def add(ids: HashMap[String,Double]) = {
    for (entry <- ids){
        ranking += ((entry._1,ranking.get(entry._1).getOrElse(0.0)+entry._2))
    }
  }


}
