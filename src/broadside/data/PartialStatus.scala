/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.data

import scala.collection.mutable

/**
  * Created by luk on 8/11/16.
  */
class PartialStatus(val errorCondition: Boolean,val technical: Boolean,val info: String) extends java.io.Serializable{
  val partialTotalEffects = new mutable.HashMap[String, Double]
  val partialMainEffects = new mutable.HashMap[String, Double]
  val partialInteractionEffects = new mutable.HashMap[(String,String), Double]
  val partialFeatureProjections = new mutable.HashMap[String,Int]
  var permutations: Boolean = false
  var d1: Boolean = false

  var timeTrain: Long = 0
  var timeScoreFeatures: Long = 0
  var timeScoreInteractions: Long = 0
  var timeTotal: Long = 0
}

object PartialStatus{

  def standard = new PartialStatus(false,false,"")
  def technical(info: String) = new PartialStatus(false,true,info)
  def error(info: String) = new PartialStatus(true,true,info)

}
