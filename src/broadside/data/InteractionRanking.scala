/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.data

import broadside.api.{FeatureEntry, InteractionEntry}
import scala.collection.mutable.HashMap

/**
  * Created by luk on 7/8/16.
  */
class InteractionRanking extends Serializable {
  var ranking = new HashMap[(String,String),Double]


  def this(other: InteractionRanking) = {
    this
    ranking = other.ranking.clone()
  }

  def flush  = {
    ranking.clear
    ranking = new HashMap[(String,String),Double]
  }

  def add(ids: HashMap[(String,String),Double]) = {
    for (entry <- ids){
      ranking += (((entry._1._1,entry._1._2),entry._2 + ranking.get((entry._1._1,entry._1._2)).getOrElse(0.0)))
    }
  }

  def byID(p_interactions: Int = Int.MaxValue) : Array[InteractionEntry]= {
    ranking.toSeq.
      sortWith((v1,v2) => v1._2 > v2._2).
      zipWithIndex.
      filter(v => v._2 < p_interactions).
      map( v=> {
        new InteractionEntry(v._2,v._1._1._1,v._1._1._2,v._1._2)

      }).toArray
  }


}

object InteractionRanking{

  def distance(oldRanking: InteractionRanking, newRanking: InteractionRanking, p_interactions: Integer): Double = {
    if (p_interactions==0)
      return(Double.PositiveInfinity)

    val oldPositions = oldRanking.byID(p_interactions)
    if  (oldPositions.length < p_interactions)
      return Double.PositiveInfinity

    val newPositions: Array[InteractionEntry] = newRanking.byID(Int.MaxValue)

    val newPositionsDictionary = new HashMap[(String,String),Int]

    newPositions.map( pos => newPositionsDictionary += (( (pos.sourceFeature,pos.targetFeature),pos.ranking)))

    var cumulatedDistance = 0.0

    for (pos <- oldPositions){
      val oldRankingPos = pos.ranking
      val newRankingPos = newPositionsDictionary.get((pos.sourceFeature,pos.targetFeature)).getOrElse(-1)
      if (newRankingPos == -1) return Double.PositiveInfinity
      cumulatedDistance += math.abs(newRankingPos-oldRankingPos)
    }

    val distance = cumulatedDistance / p_interactions
    distance
  }
}
