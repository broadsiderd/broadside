/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.data



/**
 * Created by luk on 10/31/15.
 */
abstract class Dataset(path: String, decisionFirst: Boolean, isTransposed: Boolean, hasHeader: Boolean, hasDatatypeInfo: Boolean) extends Serializable{

  val nominalDict = new NominalDictionary
  val separator = ";" //deal with it

  //def getLocalInstanceRaw: Option[Array[String]]
  //def getLocalInstanceRowOriented: Option[Array[Observation]]
  def getLocalInstance: Option[Array[Feature]]

  def copy: Dataset


  def validate: String
}

object Dataset{
  val separator=";"
  def getValidNames = Array("LOCAL")

  def apply(
             name: String,
             path: String,
             decisionFirst: Boolean,
             isTransposed: Boolean,
             hasHeader: Boolean,
             hasDatatypeInfo: Boolean
             ): Option[Dataset] = {

    if (name=="LOCAL")
      Some(new DatasetLocal(
                path,
                decisionFirst,
                isTransposed,
                hasHeader,
                hasDatatypeInfo
      ))
    else
      None
  }
}