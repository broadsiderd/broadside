/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.data

import broadside.api.{InteractionEntry, FeatureEntry, Status}

/**
 * Created by luk on 10/31/15.
 */
object StatusFactory {
  def error(message: String): Status = new Status(message,true)
  def info(message: String): Status = new Status(message,false)



  def standard(ri: Array[FeatureEntry], id: Array[InteractionEntry], s: Integer, fl: Array[String], distances: Array[Double], distancesInteractions: Array[Double]): Status = {
    val status = new Status
    status.featureRanking = ri
    status.interactionRanking = id
    status.s = s
    status.fullFeatureList = fl
    status.distance = distances(distances.length-1)
    status.distances = distances
    status.distanceInteractions = distancesInteractions(distancesInteractions.length-1)
    status.distancesInteractions = distancesInteractions

    status
  }

  def perm(ri: Array[FeatureEntry], id: Array[InteractionEntry], s: Integer, fl: Array[String], distances: Array[Double], distancesInteractions: Array[Double] , pf: Double): Status = {
    val status = standard(ri,id,s,fl,distances,distancesInteractions)
    status.permutations = true
    status.permutationFraction = pf
    status
  }

  def status0: Status = {
    val status = new Status
    status.s = 0
    status
  }



}
