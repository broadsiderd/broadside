/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.data

/**
 * Created by luk on 11/2/15.
 */
object DataType{
  sealed trait Enum
  case object Nominal extends Enum
  case object Ordinal extends Enum
  case object Discrete extends Enum
  case object Real extends Enum
  case object Survival extends Enum
  case object Unknown extends Enum
  def getType(char: String) = {
    char match{
      case "N" => Nominal
      case "O" => Ordinal
      case "D" => Discrete
      case "C" => Real
      case "S" => Survival
      case _ => Unknown
    }
  }

  def getTypeChar(dataType: DataType.Enum): String = {
    dataType match{
      case Nominal => "N"
      case Ordinal => "O"
      case Discrete => "D"
      case Real => "C"
      case Survival => "S"
      case Unknown => "?"
    }
  }

  def valueToString(featName: String, featValue: Double, dataType: DataType.Enum, dct: collection.mutable.Map[String,collection.mutable.Map[Double,String]]): String = {
    if (dataType==DataType.Nominal){
      (dct.get(featName).get(featValue))
    } else {
      featValue.toString
    }
  }
}
