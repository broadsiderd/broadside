/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.data

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
 * Created by luk on 10/31/15.
 */
class DatasetLocal(
                  path: String,
                  decisionFirst: Boolean,
                  isTransposed: Boolean,
                  hasHeader: Boolean,
                  hasDatatypeInfo: Boolean
                    ) extends Dataset(
                        path,
                        decisionFirst,
                        isTransposed,
                        hasHeader,
                        hasDatatypeInfo
                      ) {

 // @transient protected var rawData: Array[String] = null
  @transient protected var colData: Array[Feature] = null
 // @transient protected var rowData: Array[Observation] = null


  def copy: Dataset = new DatasetLocal(path,decisionFirst,isTransposed,hasHeader,hasDatatypeInfo)


  override def validate: String = {
    colData = getLocalInstance.get

    val featlen = colData(0).values.length
    for (i <- 1 to colData.length -1){
      if (colData(i).values.length != featlen){
        return "ERROR: Row number " + (i.toInt+1) + " (" + colData(i).name + ") contained inconsistent number of observations."
      }
    }

    for (i <- 0 to colData.length-1){
      for (j <- 0 to colData(i).values.length-1){
        if (colData(i).values(j).isNaN){
          return "ERROR: NaNs found at row number " + (i.toInt+1) + " (" + colData(i).name + "), observation nr. " + (j.toInt+1) + ".";
        }
      }
    }

    "SUCCESS"
  }
/*
  override def getLocalInstanceRaw: Option[Array[String]] = {

    rowData = null
    colData = null

    if (rawData!=null) Some(rawData)

    try{
      val lines = Source.fromFile(path).getLines()
      val lb = new collection.mutable.ArrayBuffer[String]()
      while (lines.hasNext)
        lb+=lines.next()

      rawData = new Array[String](lb.length)
      lb.copyToArray(rawData)
      Some(rawData)
    }
    catch{
      case e: Exception => {
        e.printStackTrace()
        None
      }
    }

  }

  override def getLocalInstanceRowOriented: Option[Array[Observation]] = {
    rawData = null
    colData = null


    if (rowData!=null) return Some(rowData)

    nominalDict.flush
    getLocalInstanceRaw

    try{

      if (!isTransposed){
        val featureNames: Array[String] = if (hasHeader) rawData(0).split(separator) else {
          (1 to (rawData(0).length-1)).map("Feature" + _).asInstanceOf[Array[String]]
        }

        val featureTypes: Array[DataType.Enum] = if (hasDatatypeInfo){
          val infoIdx = if (hasHeader) 1 else 0
          rawData(infoIdx).split(separator).map(v => DataType.getType(v))
        }
        else{
          (1 to (rawData(0).length-1)).map(v => DataType.getType("?")).asInstanceOf[Array[DataType.Enum]]
        }

        featureTypes.zip(featureNames).filter(v => v._1 == DataType.Nominal).map(
          v => {
            nominalDict.addFeature(v._2)
          }
        )

        val decisionIdx = if (decisionFirst) 0 else featureNames.length-1
        val dataOffset = if (hasDatatypeInfo && hasHeader) 2
          else if (!hasDatatypeInfo && !hasHeader) 0
          else 1


        rowData = new Array[Observation](rawData.length)

        for (rowIdx <- 0 to rawData.length-1 - dataOffset){
          println(rowIdx)
          val rowInput = rawData(rowIdx+dataOffset).split(separator)
          val rowOutput = new Array[Double](rowInput.length)

          for (colIdx <- 0 to rowData.length-1){
            if (featureTypes(colIdx)==DataType.Nominal)
              rowOutput(colIdx) = nominalDict.getValueForFeature(featureNames(colIdx),rowInput(colIdx))
            else
              rowOutput(colIdx) = rowInput(colIdx).toDouble
          }

          rowData(rowIdx) = new Observation(featureNames, featureTypes, rowOutput,decisionIdx)
        }

        Some(rowData)
      }
      else{
        None
      }
    }
    catch{
      case e: Exception => {
        e.printStackTrace()
        None
      }
    }
  }
*/
  override def getLocalInstance: Option[Array[Feature]] = {
    if (colData!=null) return Some(colData)

    nominalDict.flush

    val lines = Source.fromFile(path).getLines

    val buffer = new ArrayBuffer[Feature]()

    val dataOffset = if (hasDatatypeInfo && hasHeader) 2
    else if (!hasDatatypeInfo && !hasHeader) 0
    else 1

      if (isTransposed){
        var row: Array[String] = null
        var featureCounter = 0
        var featureName = ""
        var featureType: DataType.Enum = null
        var data: Array[Double] = null
        var isDecision = false
        while(lines.hasNext){


          row = lines.next().split(separator)

          featureName = if (hasHeader) row(0) else "Feature" + featureCounter


          featureType = if (!hasDatatypeInfo) DataType.getType("?")
                        else if (hasHeader) DataType.getType(row(1))
                        else DataType.getType(row(0))

          data = new Array[Double](row.length - dataOffset)


            if (featureType == DataType.Nominal) {
              nominalDict.addFeature(featureName)
              for (idx <- 0 to data.length - 1) {
                try{
                  data(idx) = nominalDict.getValueForFeature(featureName, row(idx + dataOffset))
                }
                catch{
                  case e: Exception => {

                    //e.printStackTrace()
                    data(idx) = Double.NaN
                  }
                }

              }
            }
            else {
              for (idx <- 0 to data.length - 1) {
                try{
                  data(idx) = row(idx + dataOffset).toDouble
                }
                catch{
                  case e: Exception => {
                    //e.printStackTrace()
                    data(idx) = Double.NaN
                  }
                }

              }
            }
            isDecision = if (decisionFirst && featureCounter == 0) true
            else false

          buffer += new Feature(featureName, featureType, data, isDecision)
          featureCounter += 1

          }

        }



    if (buffer.size==0)
      None
    else{
      colData = new Array[Feature](buffer.size)
      buffer.copyToArray(colData)
      Some(colData)
    }

  }


}
