/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.data


import broadside.api.{FeatureEntry, Status}


import scala.collection.mutable.{ArrayBuffer, Seq, Map, HashMap}
import scala.math.random

/**
 * Created by luk on 11/22/15.
 */

class FeatureRanking(val ri: HashMap[String,Double],val projections: HashMap[String,Int]) extends Serializable{



  def this(other: FeatureRanking) = this(other.ri.clone(),other.projections.clone())



  def addTotal(ri: HashMap[String,Double], projections: HashMap[String,Int]) = {
    for (entry <- ri){
      this.ri += ((entry._1, entry._2 + this.ri.get(entry._1).getOrElse(0.0)))
    }

    for (entry <- projections){
      this.projections += ((entry._1, entry._2 + this.projections.get(entry._1).getOrElse(0)))
    }
  }

  def getNormalized(thres: Int, mainEffects: Option[HashMap[String,Double]] = None): Array[FeatureEntry] = {
    ri.toSeq.map(
      v => {
        val feature = v._1
        val totalEffect = v._2
        val mainEffect = if (mainEffects.nonEmpty) mainEffects.get.get(feature).getOrElse(0.0) else 0.0
        val proj = projections.get(feature).get
        (feature,totalEffect/proj,mainEffect/proj,proj)
      }
    ).sortWith((v1,v2) => (v1._2 > v2._2)).zipWithIndex.filter( v=> v._2 < thres).map(
      v => (   new FeatureEntry(v._2,v._1._1,v._1._2,v._1._3,v._1._4))

    ).toArray
  }


}

object FeatureRanking{



  def distance(oldRanking: FeatureRanking, newRanking: FeatureRanking, p: Integer): Double = {
    import scala.math.{abs, ceil}

    if (p==0)
      return(Double.PositiveInfinity)

    val oldPositions = oldRanking.getNormalized(p)
    if (oldPositions.length < p)
      return Double.PositiveInfinity

    val newPositions = newRanking.getNormalized(Int.MaxValue)
    val newPositionsDictionary = new HashMap[String,Int]
    newPositions.map ( pos => newPositionsDictionary += ((pos.feature,pos.ranking)) )


    var cumulatedDistance = 0.0
    for (pos <- oldPositions){
      val oldRankingPos = pos.ranking
      val newRankingPos = newPositionsDictionary.get(pos.feature).getOrElse(-1)
      if (newRankingPos == -1) return Double.PositiveInfinity
      cumulatedDistance += abs(newRankingPos - oldRankingPos)
    }

    val distance = cumulatedDistance / p
    distance
  }



}