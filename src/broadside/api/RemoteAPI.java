/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.api;


import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *Interface allowing to operate a Distributed Monte Carlo Feature Selection cluster remotely.
 */
public interface RemoteAPI extends Remote {

    /**
     * Allows to check if the cluster is currently busy running computations.
     * If it is not busy, then it can be configured to start new computations.
     * @return
     * INFO: TRUE<br>
     * INFO: FALSE<br>
     * ERROR: explanation
     * @throws RemoteException
     */
    String isBusy() throws RemoteException;

    /**
     * Allows to check if the cluster is ready to begin computations. This includes checking the correctness of
     * parameters and availability of the input dataset.
     * @return
     * INFO: TRUE<br>
     * INFO: FALSE explanation
     * @throws RemoteException
     */
    String isReady() throws RemoteException;


    /**
     * Should be used to aquire a list of feature selection algorithms supported by the DMCFS cluster.
     * @return
     * A list of String arrays of length 2. Each entry in the list corresponds to a different algorithm. First field
     * of an array is the identifier of the algorithm, to be used in the setAlgorithm method. Second field is the
     * description of the algorithm.<br><br>
     * example:<br>
     * [0]: TREES<br>
     * [1]: Classical Monte Carlo Feature Selection - categorical output, mixed input.
     * @throws RemoteException
     */
    List<String[]> getSupportedAlgorithms() throws RemoteException;

    /**
     * Allows to verify what feature selection algorithm is currently set in the DMCFS cluster.
     * @return
     * Algorithm identifier.
     * @throws RemoteException
     */
    String getCurrentAlgorithm() throws RemoteException;

    /**
     * Allows to change the feature selection algorithm.
     * @param algorithm Identifier of the selected algorithm obtained through getSupportedAlgorithms().
     * @return
     * SUCCESS: Algorithm set to: identifier<br>
     * ERROR: Unsupported algorithm specified.
     * @throws RemoteException
     */
    String setAlgorithm(String algorithm) throws RemoteException;

    /**
     * Should be used to see what convergence criterions are supported by the DMCFS cluster. A convergence criterion
     * controls how much samples need to be generated before a difference of two feature rankings can be calculated.
     * @return
     * A list of String arrays of length 2. Each entry in the list corresponds to a different criterion. First
     * field of the array is the convergence criterion identifier, the second is its description.<br>
     * example:<br>
     * [0]: INCREMENTAL<br>
     * [1]: Reuse samples from s-10 in s.
     * @throws RemoteException
     */
    List<String[]> getSupportedConvergenceCriterions() throws RemoteException;


    /**
     * Allows to verify what convergence criterion is currently set in the DMCFS cluster.
     * @return
     * Identifier of the convergence criterion.
     * @throws RemoteException
     */
    String getCurrentConvergenceCriterion() throws RemoteException;


    /**
     * Allows to change the convergence criterion.
     * @param criterion Identifier of the convergence criterion obtained through getSupportedConvergenceCriterions().
     * @return
     * SUCCESS: Criterion set to: identifier<br>
     * ERROR: Unsupported convergence criterion specified.
     * @throws RemoteException
     */
    String setConvergenceCriterion(String criterion) throws RemoteException;



    List<String[]> getSupportedStopCriterions() throws RemoteException;


    String getCurrentStopCriterionFeatures() throws RemoteException;
    String getCurrentStopCriterionInteractions() throws RemoteException;

    String setStopCriterionFeatures(String criterion, int threshold, double value);
    String setStopCriterionInteractions(String criterion, int threshold, double value);

    /**
     * Provides a list of all parameters valid for selected feature selection algorithm.
     * @return
     * A list of String arrays of length 4. Each entry in the list corresponds to a different parameter.<br>
     * fields of the arrays:<br>
     * [0] - name of the parameter<br>
     * [1] - current value of the parameter; numeric parameters are expressed as Strings<br>
     * [2] - type of the parameter<br>
     * [3] - description of meaning of the parameter<br><br>
     *
     * example:<br>
     * [0]: s_delta<br>
     * [1]: 10<br>
     * [2]: positive integer<br>
     * [3]: Number of samples increment in subsequent iterations.
     * @throws RemoteException
     */
    List<String[]> getParameterInfo() throws RemoteException;


    /**
     * Allows to set an algorithm parameter to a new value.
     * @param key name of the parameter
     * @param value value to which the parameter is to be set, expressed as String
     * @return
     * SUCCESS<br>
     * ERROR: explanation
     * @throws RemoteException
     */
    String setParameter(String key, String value) throws RemoteException;


    /**
     * Get a list of filesystem types which can be accessed by the worker nodes. These could include OS filesystems,
     * distributed filesystems or external databases.
     * @return
     * A list of strings containing supported filesystem type identifiers.
     * @throws RemoteException
     */
    List<String> getSupportedFilesystems() throws RemoteException;

    /**
     * Point the DMCFS cluster to an input dataset. The dataset will be accessed independently on each node of the
     * cluster. It must be visible on all nodes under the same path.
     * @param filesystem Filesystem identifier, must be one from the list provided by getSupportedFilesystems.
     * @param path A path to the input dataset.
     * @param decisionFirst True if decision vector is the first variable, false if the last one.<BR>CURRENTLY ONLY TRUE IS SUPPORTED!
     * @param isTransposed True if variables are placed in rows, false if in columns.<br>CURRENTLY ONLY TRUE IS SUPPORTED!
     * @param hasHeader True if the dataset contains variable names.<BR>CURRENTLY ONLY TRUE IS SUPPORTED!
     * @param hasDatatypeInfo True if the dataset contains information about type of variables.<BR>CURRENTLY ONLY TRUE IS SUPPORTED!
     * @return
     * SUCCESS: Dataset set.
     * ERROR: explanation
     * @throws RemoteException
     */
    String setDataset(
            String filesystem,
            String path,
            Boolean decisionFirst,
            Boolean isTransposed,
            Boolean hasHeader,
            Boolean hasDatatypeInfo
    ) throws RemoteException;



    /**
     * Starts the computations - if the cluster isn't already busy.
     * @return
     * SUCCESS<br>
     * ERROR: explanation
     * @throws RemoteException
     */
    String start() throws RemoteException;

    /**
     * Resumes computations from a checkpoint. This is useful in case of recovering after a crash.
     * @return
     * SUCCESS<br>
     * ERROR: explanation
     * @throws RemoteException
     */
    String resurrect() throws RemoteException;

    /**
     * Query the running DMCFS cluster for a buffered list of status updates. This is needed for monitoring
     * the activities of the cluster. After the query, cluster's buffer is emptied.
     * @return
     * A list of Statuses.
     * @throws RemoteException
     */
    List<Status> getStatus() throws RemoteException;

    /**
     * Reset the DMCFS cluster. This will stop all running computations and delete all configuration.
     * @return
     * SUCCESS<br>
     * ERROR: Error during reset.
     * @throws RemoteException
     */
    String reset() throws RemoteException;

    /**
     * Shuts down the Cluster.
     * @return
     * SUCCESS<br>
     * ERROR: Error on shutdown 8)
     * @throws RemoteException
     */
    String shutdown() throws RemoteException;
}