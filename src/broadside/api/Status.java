/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.api;

import java.io.Serializable;
import java.lang.Double;
import java.lang.String;
import java.time.LocalDateTime;

/**
 * This class contains information about status of computations performed by the Distributed Monte Carlo Feature
 * Selection cluster.
 * It contains both results, as well as technical updates.
 */
public final class Status implements Serializable {

    /**
     * Time at which a Status was generated.
     */
    public LocalDateTime timestamp;


    /**
     * Has a stage of computations been completed. This can mean either reaching a stable feature ranking or completing
     * permutations.
     */
    public Boolean completed;

    /**
     * Is the status containing results of running permutations.
     */
    public Boolean permutations;

    /**
     * Is the status purely technical - not containing results.
     */
    public Boolean technical;

    /**
     * Has there been an error condition.
     */
    public Boolean errorCondition;

    /**
     * Human readable explanation.
     */
    public String info;


    /**
     * What number of samples has been reached so far.
     */
    public int s;

    /**
     * What fraction of required permutation tests have been completed.
     */
    public double permutationFraction;

    /**
     * A complete list of features used in the experiment, needed for post-MCFS result analysis.
     */
    public String[] fullFeatureList;

    /**
     * The feature ranking.
     */
    public FeatureEntry[] featureRanking;

    /**
     * The interaction ranking.
     */
    public InteractionEntry[] interactionRanking;


    /**
     * Distance to the last feature ranking.
     */
    public Double distance;

    /**
     * Distances of consequent feature rankings.
     */
    public double[] distances;


    public Double distanceInteractions;
    public double[] distancesInteractions;


    public Status(){
        timestamp = LocalDateTime.now();
        technical = false;
        errorCondition = false;
        info = "";
        completed = false;
        permutations = false;
    }

    public Status(String errorInfo,Boolean error){
        this();
        technical = true;
        errorCondition = error;
        this.info = errorInfo;
    }

}

