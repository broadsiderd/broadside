/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.api;

import java.io.Serializable;

/**
 * This class represents an entry in the Interaction ranking.
 */
public class InteractionEntry implements Serializable{

    /**
     * Position in the ranking.
     */
    public int ranking;

    /**
     * Name of the Source Feature.
     */
    public String sourceFeature;

    /**
     * Name of the Target Feature.
     */
    public String targetFeature;


    /**
     * Value of Interaction Effect.
     */
    public Double id;


    /**
     * Z-Score for Interaction Effect.
     */
    public Double id_zscore;


    /**
     * Permuted values.
     */
    public double[] permutations;


    public InteractionEntry(int ranking, String sourceFeature, String targetFeature, Double id){
        this.ranking = ranking;
        this.sourceFeature = sourceFeature;
        this.targetFeature = targetFeature;
        this.id = id;
        this.permutations = new double[0];
        //this.permutations[0] = Double.NaN;
        this.id_zscore = Double.NaN;
    }

    public InteractionEntry(InteractionEntry that){
        this.ranking = that.ranking;
        this.sourceFeature = that.sourceFeature;
        this.targetFeature = that.targetFeature;
        this.id = that.id;
        this.permutations = that.permutations;
        this.id_zscore = that.id_zscore;
    }
}
