/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.api;

import java.io.Serializable;

/**
 * This class represents an entry in a Feature Ranking.
 */
public final class FeatureEntry implements Serializable{

    /**
     * Position of the Feature in the ranking.
     */
    public int ranking;

    /**
     * Name of the Feature.
     */
    public String feature;

    /**
     * Relative importance of the Feature.
     */
    public double totalEffect;


    /**
     * Robust z-score for totalEffects assessed using permutations.
     */
    public double totalEffectZScore;


    /**
     * Total effect obtained for single-feature subsets.
     */
    public double totalEffect1D;


    /**
     * Robust z-score for single-feature subset total effects.
     */
    public double totalEffect1DZScore;


    /**
     * Main effect of the feature.
     */
    public double mainEffect;


    /**
     * Permuted values.
     */
    public double[] permutations;

    /**
     * Permuted values for single-feature subsets.
     */
    public double[] permutations1D;

    /**
     * Number of times each feature was included in a feature sample.
     */
    public int projections;


    /**
     * Number of times each feature was included in a single-feature feature sample.
     */
    public int projections1D;


    public FeatureEntry(int ranking, String feature, Double totalEffect, Double mainEffect, int projections){
        this.ranking = ranking;
        this.feature = feature;
        this.totalEffect = totalEffect;
        this.totalEffectZScore = Double.NaN;
        this.totalEffect1D = Double.NaN;
        this.totalEffect1DZScore = Double.NaN;
        this.mainEffect = mainEffect;
        this.projections = projections;
        this.projections1D = 0;
        this.permutations = new double[0];
        //this.permutations[0] = Double.NaN;
        this.permutations1D = new double[1];
        this.permutations1D[0] = Double.NaN;
    }

    public FeatureEntry(FeatureEntry that){
        this.ranking = that.ranking;
        this.feature = that.feature;
        this.totalEffect = that.totalEffect;
        this.totalEffectZScore = that.totalEffectZScore;
        this.totalEffect1D = that.totalEffect1D;
        this.totalEffect1DZScore = that.totalEffect1DZScore;
        this.mainEffect = that.mainEffect;
        this.projections = that.projections;
        this.projections1D = that.projections1D;
        this.permutations = that.permutations;
        this.permutations1D = that.permutations1D;
    }
}
