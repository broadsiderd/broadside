/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.utils

object Descriptive {

  def median(vector: Array[Double]): Double = {





    val middleLow = scala.math.floor(vector.length/2).toInt -1
    val middleHigh = scala.math.ceil(vector.length/2).toInt -1
    val vectorSorted = vector.sorted

    if ( middleLow == middleHigh ){
      return vector(middleLow)
    } else {
      return ( (vector(middleHigh) + vector(middleLow  ))/2   )
    }
  }


  //def mortality()
}
