/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.utils

/**
 * Created by luk on 10/11/15.
 */
object Checkers {

  def isBoolean(value:String) = {
    try{
      val x = value.toBoolean
      "SUCCESS"

    }
    catch{
      case e: IllegalArgumentException => "ERROR: Invalid boolean data: " + value
    }
  }

  def isZero(value:String)= {
    try{
      if (value.toDouble==0.0) "SUCCESS"
      else "ERROR: Value " + value + "is not 0."
    }
    catch{
      case e: NumberFormatException => "ERROR: Invalid real character: " + value
    }
  }


  def isFraction(value: String,
                 lowerOpen:Boolean=false,
                 upperOpen:Boolean=false)={
    var lower = 0.0
    var upper = 1.0
    if (lowerOpen) lower = Double.MinPositiveValue
    if (upperOpen) upper = 1 - Double.MinPositiveValue

    isReal(value,lower=lower,upper=upper)
  }

  def isReal(
                 value: String,
                 lower: Double = Double.MinValue,
                 upper: Double = Double.MaxValue
                 ): String = {
    try{
      if ((value.toDouble >= lower) && (value.toDouble <= upper)) "SUCCESS"
      else "ERROR: Value " + value + " out of range:" + "[" + lower.toString + "," + upper.toString + "]."
    }
    catch{
      case e: NumberFormatException => "ERROR: Invalid real character " + value
    }
  }



  def isInteger(
                 value: String,
                 lower: Integer = Integer.MIN_VALUE,
                 upper: Integer = Integer.MAX_VALUE
                 ): String = {
    try{
      if ((value.toInt >= lower) && (value.toInt <= upper)) "SUCCESS"
      else "ERROR: Value " + value + " out of range:" + "[" + lower.toString + "," + upper.toString + "]."
    }
    catch{
      case e: NumberFormatException => "ERROR: Invalid integer character " + value
    }
  }
}
