/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.utils

import java.io.File
import java.io.FileOutputStream
import java.io.PrintWriter

import java.time.LocalDateTime

/**
 * Created by luk on 9/15/15.
 */
class Logger(filename: String) {
  var log = new PrintWriter(new FileOutputStream(new File(filename),true))

  def output(): Unit = {
    log.close()
    log = new PrintWriter(new FileOutputStream(new File(filename),true))
  }

  def clear(): Unit = {
    log.close
    log = new PrintWriter(new FileOutputStream(new File(filename),false))
  }

  def log(text: String): Unit = {
    log.println(LocalDateTime.now.toString + "\t" + text)
    output()
  }

  def write(text: String): Unit = {
    log.println(text)
  }

  def close(): Unit = {
    log.close
  }
}