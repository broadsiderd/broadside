/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.utils

import scala.collection.mutable.{ArrayBuffer, ArraySeq, HashSet, Set}
import scala.math.{ceil, random}

/**
 * Created by luk on 11/8/15.
 */
object Samplers {
  def randomSubset[T](set: Array[T],k: Int): Seq[T] = {

    val size = if (k>set.size) set.size else k

    val sample = new HashSet[T]()
    while (sample.size<k){
      val number = ceil(random*set.size).toInt
      if (!sample.contains(set(number-1)))
        sample.add(set(number-1))
    }

    sample.toSeq
  }

  def singleNum(max: Int): Int = math.floor(math.ceil(random * max)).toInt

  def permutation[T](orig: Array[T]): ArraySeq[T] = orig.map(v => (v,random)).sortWith((v1,v2) => v1._2 < v2._2).map(v => v._1)

  def square(n: Int) = n*n
  def nPairs(n: Int) = (square(n)-n)/2


  def combinations[T](input: Seq[T]): Seq[(T,T)] = {
    _combinations(input.toIndexedSeq)

  }

  private def _combinations[T](input: Seq[T]): Seq[(T,T)] ={
    if (input.size>1){
      val left = input(0)
      input.drop(1).map( v=> (left,v)) ++ _combinations(input.drop(1))
    }
    else
      new ArraySeq[(T,T)](0)
  }


  def avg(in: Array[Double]) = in.reduce((v1,v2)=> v1+v2) / in.length
}
