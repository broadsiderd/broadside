/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control

import java.util.concurrent.TimeoutException
import scala.concurrent.duration._
import akka.util.Timeout

import com.typesafe.config.Config

import broadside.api.Status
import broadside.data.Feature
import broadside.engines.Broadside

import akka.actor.{Actor, ActorRef, ActorLogging, Props}

/**
 * Created by luk on 10/14/15.
 */
class Worker(config: Config, id: Int, collector: ActorRef, dataActor: ActorRef) extends Actor with ActorLogging{
  import Worker._

  log.info("Worker constructor called.")
  var algorithm: Broadside = null
  var running = false
  var permuting = false
  log.info("Worker constructor completed.")

  protected def preprocess(workload: Broadside): Boolean = {
    log.info("Preprocessing.")
    algorithm = workload
    algorithm.setWorkerContext(this)
    val status = algorithm.preprocess
    collector ! Collector.InPartialStatus(status,id)

    !status.errorCondition
  }

  def getDecision: Feature = {
    try{
      val answer = Wait.wait(dataActor,DataActor.InGetDecision, Timeout(600 seconds)).asInstanceOf[DataActor.OutDecision]
      answer.decision
    }
    catch{
      case e: TimeoutException =>{
        log.error("Unable to contact Data Actor.")
        null
      }
    }
  }

  def getSample: Seq[Feature] = {
    try{
      val answer = Wait.wait(dataActor,DataActor.InGetSample, Timeout(600 seconds)).asInstanceOf[DataActor.OutSample]
      answer.features
    }
    catch{
      case e: TimeoutException =>{
        log.error("Unable to contact Data Actor.")
        null
      }
    }
  }

  def getFeatureNames: Array[String] = {
    try{
      val answer = Wait.wait(dataActor,DataActor.InGetFeatureNames, Timeout(600 seconds)).asInstanceOf[DataActor.OutFeatureNames]
      answer.featureNames
    }
    catch{
      case e: TimeoutException =>{
        log.error("Unable to contact Data Actor.")
        null
      }
    }
  }


  def receive = {

    case InStart(workload) => {
        if (preprocess(workload)){
          running = true
          self ! Auto
        }
      }


    case Auto => {
      if (running){
        val status = if (permuting) algorithm.permute else algorithm.iterate
        collector ! Collector.InPartialStatus(status,id)
        if (!permuting)
          context.parent ! Master.InCollectorNotified
        self ! Auto
      }
    }

    case InStop => {
      running = false
      permuting = false
      log.info("Worker stopping.")
    }

    case InPermute(workload) => {
      log.info("Starting permutations.")
      running = true
      permuting = true

      if (workload!=null){
        if (preprocess(workload))
          self ! Auto
      }else
        self ! Auto

    }
      
  }
}

object Worker {
  case object Auto
  case object InStop
  case class InStart(workload: Broadside)
  case class InPermute(workload: Broadside)
  case class OutStatusUpdate(status: Status, sender: Integer)
}