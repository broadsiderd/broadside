/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control.mailboxes;

import java.util.concurrent.ConcurrentLinkedQueue

import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.dispatch.Envelope
import akka.dispatch.MailboxType
import akka.dispatch.MessageQueue
import akka.dispatch.ProducesMessageQueue
import com.typesafe.config.Config
import broadside.control.WriterActor


trait WriterActorMailboxSemantics

object WriterActorMailbox{
  class WriterActorQueue extends MessageQueue with WriterActorMailboxSemantics{

    private final val backupQueue = new ConcurrentLinkedQueue[Envelope]()
    private final val statusQueue = new ConcurrentLinkedQueue[Envelope]()

    // these should be implemented; queue used as example
    def enqueue(receiver: ActorRef, handle: Envelope): Unit = {
      handle.message match {
        case WriterActor.InWriterActorCollectorBackup(controllerBody) => {
          backupQueue.clear()
          backupQueue.offer(handle)
        }

        case WriterActor.InWriterActorHandle(status) => {
          statusQueue.clear()
          statusQueue.offer(handle)
        }

        case default => {
          val x= 1
        }
      }
    }

    def dequeue(): Envelope = {
      if (!backupQueue.isEmpty)
        backupQueue.poll
      else
        statusQueue.poll
    }
    def numberOfMessages: Int = backupQueue.size + statusQueue.size
    def hasMessages: Boolean = !backupQueue.isEmpty || !statusQueue.isEmpty
    def cleanUp(owner: ActorRef, deadLetters: MessageQueue) {
      while (hasMessages) {
        deadLetters.enqueue(owner, dequeue())
      }
    }
  }
}


/**
 * Created by luk on 8/7/16.
 */
class WriterActorMailbox extends MailboxType with ProducesMessageQueue[WriterActorMailbox.WriterActorQueue] {
  import WriterActorMailbox._
  // This constructor signature must exist, it will be called by Akka
  def this(settings: ActorSystem.Settings, config: Config) = {
    // put your initialization code here
    this()
  }

  // The create method is called to create the MessageQueue
  final override def create(
                             owner:  Option[ActorRef],
                             system: Option[ActorSystem]): MessageQueue = new WriterActorQueue

}
