/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control

import com.typesafe.config.Config
import broadside.api.{RemoteAPI, Status}
import broadside.data.{Dataset, StatusFactory}
import broadside.utils.Logger
import broadside.engines.Broadside
import java.lang.Boolean
import java.util
import java.util.concurrent.TimeoutException

import scala.concurrent.duration._
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.util.Timeout
import broadside.control.stopcriterions.StopCriterion




/**
 * Created by luk on 9/15/15.
 */
class Listener(config: Config) extends RemoteAPI{
  var actorSystem: ActorSystem = null
  var props: Props = null
  var master: ActorRef = null


  var featureSelectorName: String = null
  var featureSelector: Broadside = null
  var convergenceCriterion: ConvergenceCriterion.Enum = null
  var stopCriterionFeatures: stopcriterions.StopCriterion = null
  var stopCriterionInteractions: stopcriterions.StopCriterion = null
  setup


  private def setup = {
    actorSystem = ActorSystem(config.getString("dmcfs.cluster.system-name"),config)
    props = Props(classOf[Master],config)
    featureSelectorName = "CLASSIFICATIONTREES"
    featureSelector = Broadside(featureSelectorName).get
    convergenceCriterion = ConvergenceCriterion.Incremental


    stopCriterionFeatures = stopcriterions.StopCriterion(
      config.getString("dmcfs.defaults.stopcriterion.features.method"),
      config.getInt("dmcfs.defaults.stopcriterion.features.threshold"),
      config.getDouble("dmcfs.defaults.stopcriterion.features.value")
    ).get

    stopCriterionInteractions = stopcriterions.StopCriterion(
      config.getString("dmcfs.defaults.stopcriterion.interactions.method"),
      config.getInt("dmcfs.defaults.stopcriterion.interactions.threshold"),
      config.getDouble("dmcfs.defaults.stopcriterion.interactions.value")
    ).get

    master = actorSystem.actorOf(props,"Master")
  }

  private def _reset = {
    actorSystem.terminate()
    setup
  }


  override def isBusy: String = {

    try{
      val info = Wait.wait(master,Master.InIsBusy,Timeout(6000 seconds)).asInstanceOf[Master.OutIsBusy]
      if (info.busy==true) "INFO: TRUE"
      else "INFO: FALSE"
    }
    catch{
      case e: TimeoutException => "ERROR: No reply from Master."
    }

  }

  override def isReady: String = {
    val busy = isBusy
    if (busy.contains("ERROR"))
      return busy

    if (busy.contains("TRUE"))
      return "FALSE: Cluster is busy."

    return featureSelector.isReady
  }


  override def start: String = {
    if (isBusy.contains("TRUE")) return "ERROR: Unable to start, busy."


    if (!isReady.contains("TRUE")) return "ERROR: Improper configuration."


    try{
      val info = Wait.wait(master, Master.InStart(featureSelector,convergenceCriterion,stopCriterionFeatures,stopCriterionInteractions), Timeout(6000 seconds)).asInstanceOf[Master.OutStarted]
      if (info.status==true) "SUCCESS"
      else "ERROR: " + info.info
    }
    catch {
      case e: TimeoutException => "ERROR: No reply from Master."
    }

  }

  override def resurrect: String = {
    try{
      val info = Wait.wait(master,Master.InResurrect, Timeout(6000 seconds)).asInstanceOf[Master.OutStarted]
      if (info.status==true) "SUCCESS"
      else "ERROR: " + info.info
    }
    catch{
      case e: TimeoutException => {
        "ERROR: No reply from Master."
      }
    }
  }

  override def getStatus: java.util.List[Status] = {
    try{
      val info = Wait.wait(master,Master.InGetStatusStack, Timeout(6000 seconds)).asInstanceOf[Master.OutStatusStack]
      info.sstack.asInstanceOf[java.util.List[Status]]
    }
    catch{
      case e: TimeoutException => {
        val list = new java.util.LinkedList[Status]
        list.add(StatusFactory.error("ERROR: Communication error."))
        list
      }
    }
  }

  override def reset: String = {
    try{
      _reset
      "SUCCESS"
    }
    catch{
      case e: TimeoutException => "ERROR: Error during reset."
    }
  }

  override def shutdown: String = {
    try {
      actorSystem.terminate
      "SUCCESS"
    }
    catch {
      case e: TimeoutException => "ERROR: Error on shutdown 8)"
    }
  }



  override def getSupportedAlgorithms: java.util.List[Array[String]] = {
    val algList = new java.util.LinkedList[Array[String]]
    val info = Broadside.getValidNames

    for (line <- info){
      val alg = new Array[String](line.length)
      line.copyToArray(alg)
      algList.add(alg)
    }
    algList.asInstanceOf[java.util.List[Array[String]]]
  }

  override def getCurrentAlgorithm: String = featureSelectorName

  override def setAlgorithm(algorithm: String): String = {
    val newFeatureSelector = Broadside(algorithm)

    if (newFeatureSelector.isEmpty) "ERROR: Unsupported algorithm specified."
    else{
      newFeatureSelector.get.dataset = featureSelector.dataset
      featureSelector = newFeatureSelector.get
      featureSelectorName = algorithm
      "SUCCESS: Algorithm set to: " + algorithm
    }

  }

  override def getSupportedConvergenceCriterions: java.util.List[Array[String]] = {
    val retList = new java.util.LinkedList[Array[String]]

    val crits = ConvergenceCriterion.getValidNames

    for (crit <- crits){
      retList.add(crit)
    }

    retList
  }

  override def setConvergenceCriterion(criterion: String): String = {
    val crit = ConvergenceCriterion(criterion)
    if (criterion == ConvergenceCriterion.Unknown)
      "ERROR: Unsupported convergence criterion specified."
    else{
      convergenceCriterion = crit
      "SUCCESS: Convergence criterion set to: " + convergenceCriterion.toString
    }

  }

  override def getCurrentConvergenceCriterion: String = convergenceCriterion.toString


  override def getSupportedStopCriterions: util.List[Array[String]] = {
    val retList = new util.LinkedList[Array[String]]

    val crits = stopcriterions.StopCriterion.getValidNames

    for (crit <- crits)
      retList.add(crit)

    retList
  }

  override def getCurrentStopCriterionFeatures: String = stopCriterionFeatures.getCurrent

  override def getCurrentStopCriterionInteractions: String = stopCriterionInteractions.getCurrent


  override def setStopCriterionFeatures(criterion: String, threshold: Int, value: Double): String = {
    val crit  = StopCriterion(criterion,threshold,value)

    if (crit.isEmpty)
      "ERROR: Wrong stop criterion configuration."
    else{
      stopCriterionFeatures = crit.get
      "SUCCESS: Stop criterion (features) set to: " + stopCriterionFeatures.getCurrent
    }
  }

  override def setStopCriterionInteractions(criterion: String, threshold: Int, value: Double): String = {
    val crit  = StopCriterion(criterion,threshold,value)

    if (crit.isEmpty)
      "ERROR: Wrong stop criterion configuration."
    else{
      stopCriterionInteractions = crit.get
      "SUCCESS: Stop criterion (interactions) set to: " + stopCriterionInteractions.getCurrent
    }
  }


  override def getParameterInfo(): java.util.List[Array[String]] = {
    val info = featureSelector.getParameterInfo

    val pList = new java.util.LinkedList[Array[String]]()

    for (i <- info){
      val paramameter = new Array[String](i.length)
      i.copyToArray(paramameter)
      pList.add(paramameter)
    }
    pList.asInstanceOf[java.util.List[Array[String]]]

  }

  override def setParameter(key: String, value: String): String = featureSelector.setParameter(key,value)



  override def getSupportedFilesystems :java.util.List[String] = {
    val names = Dataset.getValidNames
    val nameList = new java.util.LinkedList[String]
    for (name <- names){
      nameList.add(name)
    }

    nameList.asInstanceOf[java.util.List[String]]
  }

  override def setDataset(
                           filesystem: String,
                           path: String,
                           decisionFirst: Boolean,
                           isTransposed: Boolean,
                           hasHeader: Boolean,
                           hasDatatypeInfo: Boolean
                           ) = {
    val ds = Dataset(
      filesystem,path,decisionFirst,isTransposed,hasHeader,hasDatatypeInfo
    )


    try{
      ds.get.getLocalInstance
    } catch {
        case e: Exception => {
          "ERROR: check dataset path or struccture."
        }
    }


    if (ds.nonEmpty){
      featureSelector.dataset = ds
      "SUCCESS: Dataset set."
    }
    else{
      "ERROR: Unsupported filesystem: " + filesystem
    }

  }


}
