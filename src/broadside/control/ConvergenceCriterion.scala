/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control

/**
 * Created by luk on 11/22/15.
 */
object ConvergenceCriterion {
  sealed trait Enum{
    def getCollectorBody: Option[broadside.control.CollectorBody]
  }
  case object Incremental extends Enum{
    override def toString = "INCREMENTAL"
    override def getCollectorBody= Some(new CollectorBodyIncremental)
  }
  case object Independent extends Enum{
    override def toString = "INDEPENDENT"
    override def getCollectorBody = Some(new CollectorBodyIndependent)
  }

  case object Twin extends Enum{
    override def toString = "TWIN"
    override def getCollectorBody = Some(new CollectorBodyTwin)
  }

  case object Unknown extends Enum{
    override def toString = "UNKNOWN"
    override def getCollectorBody = None
  }

  val default = Incremental

  val validLevels = Map(
    "INCREMENTAL" -> Incremental,
    "INDEPENDENT" -> Independent,
    "TWIN" -> Twin
  )


  def apply(what: String): ConvergenceCriterion.Enum = validLevels.get(what).getOrElse(Unknown)
  def getValidNames = Seq(
    Array("INCREMENTAL","Reuse samples from s-10 in s."),
    Array("INDEPENDENT","Create s samples on each iteration."),
    Array("TWIN","Compare two parallelly running processes.")
  )

}
