/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control

import broadside.data.{FeatureRanking, InteractionRanking, PartialStatus}

class CollectorBodyTwin extends CollectorBody {

  override def getConvergenceCriterion: ConvergenceCriterion.Enum = ConvergenceCriterion.Twin

  override def recalculate: Unit = {
      sNew = 0
      sOld = 0
      sOldTarget = s_initial
      sNewTarget = s_initial
      permutationsCompleted = -1
  }

  override def specificInit(status: PartialStatus): Unit = {

    if (sOld == 0) {
      oldFeatureRanking = Some(new FeatureRanking(status.partialTotalEffects, status.partialFeatureProjections))
      oldInteractionRanking = new InteractionRanking()
      oldInteractionRanking.add(status.partialInteractionEffects)
      sOld += s_delta
    }
    else if (sOld < sOldTarget) {
      oldFeatureRanking.get.addTotal(status.partialTotalEffects, status.partialFeatureProjections)
      oldInteractionRanking.add(status.partialInteractionEffects)
      sOld += s_delta
    }
    else if (sNew == 0){
      newFeatureRanking = Some(new FeatureRanking(status.partialTotalEffects, status.partialFeatureProjections))
      newInteractionRanking = new InteractionRanking()
      newInteractionRanking.add(status.partialInteractionEffects)
      mainEffectList.add(status.partialMainEffects)
      sNew += s_delta
    } else {
      newFeatureRanking.get.addTotal(status.partialTotalEffects,status.partialFeatureProjections)
      newInteractionRanking.add(status.partialInteractionEffects)
      mainEffectList.add(status.partialMainEffects)
      sNew += s_delta
    }
  }

  override def specificTurnFinish: Unit = {
    sOldTarget += s_delta
    sNewTarget += s_delta

  }

  override def specificPermutationSetup: Unit = {
    oldFeatureRanking = newFeatureRanking
    oldInteractionRanking = newInteractionRanking
    newFeatureRanking = None
  }
}
