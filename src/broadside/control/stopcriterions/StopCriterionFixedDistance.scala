/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control.stopcriterions

class StopCriterionFixedDistance(val stoppingCritetionValue: Double) extends StopCriterion {

  override def calculate(distances: Array[Double]): Boolean = {
    if (reached)
      return true

    if (distances.length == 0) return false
    else if (distances(distances.length-1) < stoppingCritetionValue) {
      reached = true
      return true
    } else
    false
  }

  override def getCurrent: String = "FIXED_DISTANCE"
}
