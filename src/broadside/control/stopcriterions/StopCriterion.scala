/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control.stopcriterions

abstract class StopCriterion extends java.io.Serializable {
  var reached = false

  def calculate(distances: Array[Double]): Boolean
  def getCurrent = "NONE"
}


object StopCriterion{

  def apply(
           stopCriterionMethod: String,
           stopCriterionThreshold: Int,
           stopCriterionValue: Double

           ): Option[StopCriterion] ={


    if (stopCriterionMethod == "FIXED_DISTANCE"){
      if (stopCriterionValue<0)
        return None
      else
        return Some( new StopCriterionFixedDistance(stopCriterionValue))

    }


    if (stopCriterionMethod == "STABLE_DISTANCE"){
      if (stopCriterionValue<0 || stopCriterionThreshold<=0)
        return None
      else
        return Some( new StopCriterionStableDistance(stopCriterionThreshold,stopCriterionValue))
    }

    if (stopCriterionMethod == "NO_IMPROVEMENT"){
      if (stopCriterionThreshold<=0)
        return None
      else
        return Some(new StopCriterionNoImprovement(stopCriterionThreshold))
    }

    if (stopCriterionMethod == "NONE"){
      return Some( new StopCriterionNone)
    }

    return None

  }


  def getValidNames = Seq(
    Array("FIXED_DISTANCE","The algorithm will stop when the distance between 2 consequent feature rankings will drop below a fixed value"),
    Array("STABLE_DISTANCE","The algorithm will stop when a regression line defined by n recent feature distance will become 'flat'."),
    Array("NO_IMPROVEMENT","The algorithm will stop when no improvement is observed in ranking distances in a given threshold.")
  )
}