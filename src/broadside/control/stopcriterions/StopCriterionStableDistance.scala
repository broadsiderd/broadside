/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */


package broadside.control.stopcriterions

import org.apache.commons.math3.stat.regression.SimpleRegression


class StopCriterionStableDistance(val stoppingCritetionThreshold: Int, val stoppingCriterionValue: Double) extends StopCriterion {
  val regression = new SimpleRegression


  override def calculate(distances: Array[Double]): Boolean = {
    if (reached)
      return true


    regression.clear

    if (distances.length < stoppingCritetionThreshold)
      return false

    val xStart = distances.length - stoppingCritetionThreshold
    // if (xStart<0) xStart = 0

    val xStop = distances.length - 1


    if (distances.zipWithIndex.filter( v => ( v._2 >= xStart )).map( v => v._1).reduce( _ + _) ==0 ){
      reached = true
      return true
    }




    for (i <- xStart to xStop  )
      regression.addData(i,distances(i))

    if (regression.getR.isNaN)
      return false

    val r = regression.getSlope

    if (math.abs(regression.getR) < stoppingCriterionValue){
      reached = true
      return true
    }
    else {
      return false
    }

  }

  override def getCurrent: String = "STABLE_DISTANCE"
}
