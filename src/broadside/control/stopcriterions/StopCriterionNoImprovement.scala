/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control.stopcriterions

class StopCriterionNoImprovement(val stoppingCritetionThreshold: Int) extends StopCriterion{
  var bestDist = Double.MaxValue
  var nWithoutImprovement = 0

  override def calculate(distances: Array[Double]): Boolean = {

    if (reached)
      return true

    if (distances(distances.length-1)<bestDist){
      bestDist = distances(distances.length-1)
      nWithoutImprovement = 0
    }
    else if (distances(distances.length-1) != Double.PositiveInfinity)
      nWithoutImprovement += 1

    if (nWithoutImprovement>=stoppingCritetionThreshold){
      reached = true
      return true
    }
    else
      return false



  }


  override def getCurrent: String = "NO_IMPROVEMENT"
}
