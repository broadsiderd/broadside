/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control


import java.io.{File, FileOutputStream, PrintWriter}

import akka.actor.ActorSystem
import broadside.api.{RemoteAPI, Status}
import broadside.utils.{Checkers, LogFormatter, Logger}
import com.typesafe.config.{Config, ConfigException, ConfigFactory, ConfigValueType}

import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._
import java.nio.file.Files
import java.net.InetAddress
import java.rmi.registry.LocateRegistry
import java.rmi.server.{ExportException, UnicastRemoteObject}
import java.time.LocalDateTime
import java.util

import broadside.data.{DataType, Dataset}

import scala.io.Source





/**
 * Created by luk on 9/15/15.
 */
object Entry {


    var master = false
    def role = if (master) "MASTER" else "SLAVE"
    var batch = false
    var resurrect = false
    var resultset = false

    var localPort = -1
    var localRecognizableHostname = "localhost"

    def main(args: Array[String]):Unit = {

      welcome

      if (!checkArgs(args)) return

      val config = getConfig


      if (!master){
        System.out.println(LogFormatter.fmt("Starting as a BOOSTER node."))
        val actorSystem = ActorSystem(config.getString("dmcfs.cluster.system-name"),config)
      }
      else if (resultset){
        System.out.println(LogFormatter.fmt("Creating resultset."))
        batchMode(config,null,resurrect, true)
      } else {
        val listener = new Listener(config)

        if (batch)
          batchMode(config,listener,resurrect,false)
        else
          setupRMI(config,listener,resurrect)

      }
  }

  private def checkArgs(args: Array[String]): Boolean = {
    if (args.size==0 || ((args(0) != "BATCH") && (args(0) != "SERVER") && (args(0) != "JOIN"))){
      println(LogFormatter.fmt("ERROR: Expecting SERVER, BATCH or JOIN as first argument."))
      help
      return false
    }

    if (args(0)!="JOIN") master=true
    if (args(0)=="BATCH") batch=true

    if (args.size==1) return true

    if ( (args(1) != "RESUME") && (args(1) != "RESULTSET") && !isNatural(args(1))){
      println(LogFormatter.fmt("ERROR: Expecting RESUME or port as second argument"))
      help
      return false
    }

    if (isNatural(args(1)))
      localPort=args(1).toInt
    else if (args(1) == "RESULTSET"){
      resultset = true
    } else{
      resurrect = true
    }


    if (args.size==2) return true


    if (isNatural(args(1))){
      println(LogFormatter.fmt("ERROR: To many command line arguments."))
      help
      return false
    }

    if (!isNatural(args(2))){
      println(LogFormatter.fmt(LogFormatter.fmt("ERROR: Expecting a port as 3rd argument.")))
      help
      return false
    }

    localPort = args(2).toInt

    true
  }

  private def welcome = {
    println(LogFormatter.fmt("Welcome to Broadside!"))
    println(LogFormatter.fmt("Copyright (c) 2015-2020 by Lukasz Krol."))
    println(LogFormatter.fmt("Uses JSAT 0.0.5 by Edward Raff."))
  }

  private def help = {
    println(LogFormatter.fmt("Command line arguments: [mode] [RESUME/RESULTSET] [port]"))
    println(LogFormatter.fmt("mode - in what mode to start the software"))
    println(LogFormatter.fmt("\tSERVER - application binds in RMI and waits for calls"))
    println(LogFormatter.fmt("\tBATCH  - do not register in RMI, start computations based on the configuration file"))
    println(LogFormatter.fmt("\tJOIN   - connect to Master specified in the configuration file"))
    println(LogFormatter.fmt("BATCH RESUME - resume computations from previous checkpoint (in case of crash)"))
    println(LogFormatter.fmt("BATCH RESULTSET - create output dataset for detailed analysis in Tackle"))
    println(LogFormatter.fmt("port - override the port for Akka Cluster node to bind to"))

  }


  private def isNatural(str: String): Boolean = {
    try{
      val num = str.toInt
      if (num<0) false else true
    }
    catch{
      case e: NumberFormatException => false
    }
  }

  private def getConfig: Config = {
    val clusterConfig = ConfigFactory.load("cluster")
    val akkaPort = clusterConfig.getInt("dmcfs.cluster.akka-port")
    var akkaHost = clusterConfig.getString("dmcfs.cluster.master-host")
    val systemName = clusterConfig.getString("dmcfs.cluster.system-name")

    if ((akkaHost=="self") | master) akkaHost = InetAddress.getLocalHost.getHostName


    val seedNodePath = "\"akka.tcp://" + systemName +"@" + akkaHost + ":" + akkaPort + "\""


    localPort = if ((localPort== -1) || master) akkaPort else localPort
    localRecognizableHostname = InetAddress.getLocalHost.getHostName

    if(clusterConfig.hasPath("dmcfs.cluster.hosts."+localRecognizableHostname)){
      localRecognizableHostname = clusterConfig.getString("dmcfs.cluster.hosts."+localRecognizableHostname)
    }

    ConfigFactory.parseString("akka.remote.netty.tcp.port=" + localPort).
      withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + localRecognizableHostname)).
      withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [" + seedNodePath + "]")).
      withFallback(ConfigFactory.parseString("akka.cluster.roles = [" + role + "]")).
      withFallback(ConfigFactory.load()).
      withFallback(clusterConfig).
      withFallback(ConfigFactory.load("batch")).
      withFallback(ConfigFactory.load("defaults"))

  }



  private def checkUpdates(api: Listener): java.util.List[Status] = {
    val isRunning: Boolean = api.isBusy.contains("TRUE")
    val updates: java.util.List[Status] = api.getStatus
    if (isRunning == false && updates.size == 0) return null
    else return updates
  }

  private def fileSwap(path: String) = {
    val oldfile = new File(path)
    val oldfileBackup = new File(oldfile+".old")
    if (oldfileBackup.exists())
      Files.delete(oldfileBackup.toPath)
    Files.copy(oldfile.toPath,oldfileBackup.toPath)

  }


  private def batchMode(config: Config, listener: Listener, resume: Boolean, resultset: Boolean = false): Unit = {

    System.out.println(LogFormatter.fmt("Running Broadside in batch mode."))
    val mconf = config.getConfig("dmcfs.batch.mandatory")
    val oconf = config.getConfig("dmcfs.batch.overrides")
    var paths: util.List[String] = null
    var suffixes: util.List[String] = null
    var multipath = false

    if (!resume) {
      try {
        paths = mconf.getStringList("dataset.path")
        multipath = true
      }
      catch {
        case e: ConfigException => {
          paths = new util.ArrayList[String](1)
          paths.add(mconf.getString("dataset.path"))
          multipath = false
        }
      }
    }

    if (!multipath |  resume) {
      suffixes = new util.ArrayList[String](1)
      suffixes.add(0,"")
    }
    else {
      suffixes = new util.ArrayList[String](paths.size)
      for (i <- 1 to paths.size) {
        val suffix = '_' + paths.get(i - 1).split('/').last.split('\\').last.split('.')(0)
        suffixes.add(i - 1, suffix)
      }
    }

    if (!resultset){
      if (!resume) {
        for (batchID <- 0 to (suffixes.size-1)) {
          System.out.println(LogFormatter.fmt("Processing: " + paths.get(batchID)))
          batchModeListenerSetup(listener,paths.get(batchID),mconf,oconf)
          listener.start
          batchModeOversee(listener,mconf,suffixes.get(batchID))
          createResultSet(mconf,paths.get(batchID),suffixes.get(batchID))
          //listener.reset
        }
      } else {
        System.out.println(LogFormatter.fmt("Resuming computations."))
        listener.resurrect
        batchModeOversee(listener,mconf,suffixes.get(0))
      }
    }
    else {
      for (batchID <- 0 to (suffixes.size-1)) {
        createResultSet(mconf,paths.get(batchID), suffixes.get(batchID))
        //listener.reset
      }
    }



    System.out.println(LogFormatter.fmt("Shutting down Akka."))
    listener.shutdown
    System.exit(0)

  }

  def batchModeListenerSetup(listener: Listener, path: String, mconf: Config, oconf: Config): Unit = {
        //ALGORITHM
        System.out.println(LogFormatter.fmt("We will now set the parameter 'algorithm'"))
        val validAlgorithms = listener.getSupportedAlgorithms
        System.out.println(LogFormatter.fmt("The following algorithms are currently supported (KEY - VALUE)"))
        for (i <- 0 to validAlgorithms.size() - 1) {
          System.out.println(LogFormatter.fmt("\t" + validAlgorithms.get(i)(0) + " - " + validAlgorithms.get(i)(1)))
        }

        System.out.println(LogFormatter.fmt("Using: " + mconf.getString("algorithm")))
        System.out.println(LogFormatter.fmt(listener.setAlgorithm(mconf.getString("algorithm"))))


        //DATASET
        System.out.println(LogFormatter.fmt("We will now set the input dataset path. This relative or absolute path should beaccessible on each node."))
        System.out.println(LogFormatter.fmt("This is not a validation step, it will be performed independently on each node."))

        System.out.println(LogFormatter.fmt("Using: '" + path + "'"))
        System.out.println(LogFormatter.fmt(listener.setDataset(
        "LOCAL",
          path,
        true,
        true,
        true,
        true
        )))


        //Convergence criterion
        System.out.println(LogFormatter.fmt("We will now set the convergence criterion (how the results of feature sampling will be arranged for comparisons)."))
        System.out.println(LogFormatter.fmt("The following convergence criterions are currently supported (KEY - VALUE)"))
        val validStopCriterions = listener.getSupportedConvergenceCriterions
        for (i <- 0 to validStopCriterions.size() - 1) {
        System.out.println(LogFormatter.fmt("\t" + validStopCriterions.get(i)(0) + " - " + validStopCriterions.get(i)(1)))
      }
        System.out.println(LogFormatter.fmt("Using: " + mconf.getString("stopcriterion.approach")))
        System.out.println(LogFormatter.fmt(listener.setConvergenceCriterion(mconf.getString("stopcriterion.approach"))))


        //Stop CRITERION (features)
        System.out.println(LogFormatter.fmt("We will now set the feature ranking based stop criterion."))
        System.out.println(LogFormatter.fmt("METHOD: '" + mconf.getString("stopcriterion.features.method") + "'"))
        System.out.println(LogFormatter.fmt("THRESHOLD: '" + mconf.getString("stopcriterion.features.threshold") + "'"))
        System.out.println(LogFormatter.fmt("VALUE: '" + mconf.getString("stopcriterion.features.value") + "'"))

        System.out.println(LogFormatter.fmt(
        listener.setStopCriterionFeatures(mconf.getString("stopcriterion.features.method"), mconf.getInt("stopcriterion.features.threshold"), mconf.getDouble("stopcriterion.features.value"))
        ))

        //Stop CRITERION (interactions)
        System.out.println(LogFormatter.fmt("We will now set the interaction ranking based stop criterion."))
        System.out.println(LogFormatter.fmt("METHOD: '" + mconf.getString("stopcriterion.interactions.method") + "'"))
        System.out.println(LogFormatter.fmt("THRESHOLD: '" + mconf.getString("stopcriterion.interactions.threshold") + "'"))
        System.out.println(LogFormatter.fmt("VALUE: '" + mconf.getString("stopcriterion.interactions.value") + "'"))

        System.out.println(LogFormatter.fmt(
        listener.setStopCriterionInteractions(mconf.getString("stopcriterion.interactions.method"), mconf.getInt("stopcriterion.interactions.threshold"), mconf.getDouble("stopcriterion.interactions.value"))
        ))


        //OTHERS
        System.out.println(LogFormatter.fmt("Setting additional parameters."))
        val argsLeft = oconf.entrySet().iterator()
        val pinfo = listener.getParameterInfo
        while (argsLeft.hasNext) {
          val arg = argsLeft.next

          var argType = ""
          var argDescription = ""
          for (i <- 0 to pinfo.size() - 1) {
            if (pinfo.get(i)(0) == arg.getKey) {
              argType = pinfo.get(i)(2)
              argDescription = pinfo.get(i)(3)
            }
          }


          System.out.println(LogFormatter.fmt("We will now set the parameter " + arg.getKey + "."))
          System.out.println(LogFormatter.fmt("Type: ") + argType)
          System.out.println(LogFormatter.fmt("Description: ") + argDescription)
          System.out.println(LogFormatter.fmt("Used value:" + arg.getValue))

          System.out.println(LogFormatter.fmt("Result: " + listener.setParameter(arg.getKey, arg.getValue.atKey("x").getString("x"))))
      }
  }


  def batchModeOversee(listener: Listener, mconf: Config, suffix: String): Unit ={
    val distanceFile = mconf.getString("files.ranking-distances") + suffix + ".csv"
    val riFile = mconf.getString("files.total-effect") + suffix + ".csv"
    val idFile = mconf.getString("files.interaction-effect") + suffix + ".csv"
    //val featFile = mconf.getString("files.selected-features") + suffix + ".csv"

    System.out.println(LogFormatter.fmt("Starting computations."))
    val dfCleaner = new Logger(distanceFile)
    dfCleaner.clear()
    dfCleaner.write("time\titeration\tdistance")
    dfCleaner.close()
    val rfCleaner = new Logger(riFile)
    rfCleaner.clear()
    rfCleaner.close()
    val idCleaner = new Logger(idFile)
    idCleaner.clear()
    idCleaner.close()
    var lastStatus: Status = null
    var permStatus: Status = null


    var counter = 0

    var batchFinished = false

    breakable {
      while (true) {
        counter += 1

        Thread.sleep(5000)
        var updates: java.util.List[Status] = checkUpdates(listener)
        if (updates == null) {
          break
        }

        val newDistancesIterator = updates.iterator()
        var newDistances = new ListBuffer[(Int, Double, Double, LocalDateTime)]
        while (newDistancesIterator.hasNext) {
          val status = newDistancesIterator.next()
          if (!status.permutations && !status.errorCondition && !status.technical)
            newDistances += ((status.s, status.distance, status.distanceInteractions, status.timestamp))
        }
        newDistances = newDistances.sortWith((v1, v2) => (v1._1 < v2._1))

        if (newDistances.size > 0) {
          fileSwap(distanceFile)
          val distanceWriter = new Logger(distanceFile)
          for (d <- newDistances) {
            distanceWriter.write(d._4.toString + "\t" + d._1 + "\t" + d._2 + "\t" + d._3)
          }
          distanceWriter.close
        }


        for (i <- 1 to updates.size) {
          if (!updates.get(i - 1).errorCondition && !updates.get(i - 1).technical)
            lastStatus = updates.get(i - 1)

          if (lastStatus.completed && ((lastStatus.permutationFraction == -1) || (lastStatus.permutationFraction == 1)))
            batchFinished = true
        }

        if (lastStatus != null) {
          val ranking = lastStatus.featureRanking
          fileSwap(riFile)
          val rankingWriter = new Logger(riFile)
          rankingWriter.clear


          if (ranking.size > 0)
            if (ranking(0).permutations.length > 0)
              rankingWriter.write(
                "feature\t" +
                  "total_effect\t" +
                  "total_effect_z_score\t" +
                  "projections\t" +
                  (1 to ranking(0).permutations.length).map(v => "permutation_" + v.toString + "\t").reduceLeft((v1, v2) => v1 + "\t" + v2)
              )
            else
              rankingWriter.write(
                "feature\t" +
                  "total_effect\t" +
                  "projections"
              )


          for (entry <- ranking) {
            if (ranking(0).permutations.length > 0)
              rankingWriter.write(
                entry.feature + "\t" +
                  entry.totalEffect + "\t" +
                  entry.totalEffectZScore + "\t" +
                  entry.projections + "\t" +
                  entry.permutations.map(v => v.toString).reduceLeft((v1, v2) => v1 + "\t" + v2)
              )
            else
              rankingWriter.write(
                entry.feature + "\t" +
                  entry.totalEffect + "\t" +
                  entry.projections
              )
          }

          rankingWriter.close()


          val ids = lastStatus.interactionRanking
          fileSwap(idFile)
          val idWriter = new Logger(idFile)
          idWriter.clear

          if (ids.length > 0)
            if (ids(0).permutations.length > 0)
              idWriter.write(
                "feature1\t" +
                  "feature2\t" +
                  "interaction_effect\t" +
                  "interaction_effect_z_score\t" +
                  (1 to ids(0).permutations.length).map(v => "permutation_" + v.toString + "\t").reduceLeft((v1, v2) => v1 + "\t" + v2)
              )
            else
              idWriter.write(
                "feature1\t" +
                  "feature2\t" +
                  "interaction_effect\t"
              )


          for (entry <- ids) {
            if (ids(0).permutations.length > 0)
              idWriter.write(
                entry.sourceFeature + "\t" +
                  entry.targetFeature + "\t" +
                  entry.id + "\t" +
                  entry.id_zscore + "\t" +
                  entry.permutations.map(v => v.toString).reduceLeft((v1, v2) => v1 + "\t" + v2)
              )
            else
              idWriter.write(
                entry.sourceFeature + "\t" +
                  entry.targetFeature + "\t" +
                  entry.id
              )
          }
          idWriter.close
        }

        if (batchFinished) {
          break
        }

      }
    }
  }

  def setupRMI(config: Config, listener: Listener, resume: Boolean): Unit = {

    System.out.println(LogFormatter.fmt("Setting Java security policy."))
    System.setProperty("java.security.policy",config.getString("rmi.policy-file"))

    System.out.println(LogFormatter.fmt("Checking security manager..."))
    if (System.getSecurityManager == null){
      System.out.println(LogFormatter.fmt("\tInstalling security manager."))
      System.setSecurityManager(new SecurityManager)
    }

    System.out.println(LogFormatter.fmt("Locating remote registry."))
    try{
      LocateRegistry.createRegistry(config.getInt("rmi.port"))
      System.out.println(LogFormatter.fmt("\tRegistry bound to " + config.getInt("rmi.port")))
    }
    catch {
      case e: ExportException => {
        println(LogFormatter.fmt("\tRegistry already exported?"))
      }
    }

    System.out.println(LogFormatter.fmt("Aquiring the remote registry."))
    val registry = LocateRegistry.getRegistry(config.getInt("rmi.port"))


    System.out.println(LogFormatter.fmt("Attempting to register application."))
    val export: RemoteAPI = listener
    val stub = UnicastRemoteObject.exportObject(export,0).asInstanceOf[RemoteAPI]
    registry.rebind(config.getString("rmi.exported-name"),stub)
    System.out.println(LogFormatter.fmt("Application registered in RMI."))

    if (resume)
      System.out.println(LogFormatter.fmt("Resuming computations."))


    if (resume)
      export.resurrect()

  }

  def createResultSet(mconf: Config, path: String, suffix: String): Unit = {


    System.out.println(LogFormatter.fmt("Loading the following feature score file: " + mconf.getString("files.total-effect") + suffix + ".csv"))
    val selectedDeatures = Source.fromFile(mconf.getString("files.total-effect" ) + suffix + ".csv").getLines().map( v=> v.split("\t")(0)).toSet

    System.out.println(LogFormatter.fmt("Loading the following dataset: " +  path))
    val dataset = Dataset("LOCAL",path,true,true,true,true).get


    val features = dataset.getLocalInstance.get.filter(v => (v.isDecision != true)).filter( v=> selectedDeatures.contains(v.name))
    val decision = dataset.getLocalInstance.get.filter(v => (v.isDecision == true))(0)


    System.out.println(LogFormatter.fmt("Creating the following feature subset: " + mconf.getString("files.selected-features") + suffix + ".csv"))

    var sf = new PrintWriter(new FileOutputStream(new File(mconf.getString("files.selected-features") + suffix + ".csv")),false)

    sf.println(decision.name + "\t" + features.map( v => v.name).reduceLeft( (v1,v2) => v1 + "\t" + v2))
    sf.println(DataType.getTypeChar(decision.featureType) + "\t" + features.map(v => DataType.getTypeChar(v.featureType)).reduceLeft((v1, v2) => v1 + "\t" + v2))

    val classDictionaryIterator = dataset.nominalDict.dict.iterator

    val reverseDict = collection.mutable.Map.empty[String,collection.mutable.Map[Double,String]]


    while (classDictionaryIterator.hasNext){
      val itElem = classDictionaryIterator.next()

      val key = itElem._1
      val values = itElem._2.toArray
      val valuesDict = collection.mutable.Map.empty[Double,String]

      values.map(v => (valuesDict += ((v._2,v._1)))   )
      reverseDict += ((key,valuesDict))
    }



    for (i <- 1 to decision.values.length){
      sf.println( DataType.valueToString( decision.name,decision.values(i-1),decision.featureType,reverseDict) +
        "\t" +
        features.map( v => DataType.valueToString(v.name,v.values(i-1),v.featureType,reverseDict)).reduceLeft( (v1,v2) => v1 + "\t" + v2)


      )
    }



    sf.close()
  }

}
