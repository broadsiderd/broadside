/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control

import broadside.api.Status
import broadside.control.Collector.{OutParameterUpdate, Resurrected}
import broadside.engines.Broadside

import scala.collection.mutable.ArrayBuffer
import com.typesafe.config.Config
import java.io.{File, FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream, Serializable}
import java.nio.file.Files
import java.util.concurrent.TimeoutException

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

import scala.concurrent.duration._
import akka.util.Timeout
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream
import broadside.data.{PartialStatus, StatusFactory}
/**
 * Created by luk on 11/14/15.
 */
class Collector(config: Config) extends Actor with ActorLogging{

  log.info("Collector constructor called.")
  var body: Option[CollectorBody] = None
  var iterationCounter = 0
  log.info("Spawning WriterActor.")
  val writerActorProps = Props(classOf[WriterActor],config)
  val writerActor = context.actorOf(writerActorProps.withDispatcher("wa-dispatcher"),"MCFSWriterActor")
  log.info("Collector constructor completed.")

  def error(info: String) = log.error(info)
  def info(info: String) = log.info(info)
  def warn(info: String) = log.warning(info)

  def send(msg: Status): Unit = {
    context.parent ! Worker.OutStatusUpdate(msg,0)
    if (config.getBoolean("dmcfs.cluster.files.status-serialize") )
      writerActor ! WriterActor.InWriterActorHandle(msg)
  }

  def provideFullFeatureList: Array[String] = {
    try{
      val dataActor = Wait.wait(context.parent,Master.InGetDataActor, Timeout(15 seconds)).asInstanceOf[Master.OutDataActor].dataActor
      if (dataActor.isEmpty){
        return null
      }
      else{
        return Wait.wait(dataActor.get,DataActor.InGetFeatureNames, Timeout(6000 seconds)).asInstanceOf[DataActor.OutFeatureNames].featureNames
      }

    }
    catch{
      case e: TimeoutException => null
      case e: Exception => {
        e.printStackTrace()
        null
      }
    }


  }

  override def preStart: Unit = {
    body = ConvergenceCriterion.default.getCollectorBody
    body.get.setParent(this)
  }

  def setupWorkload(workload: Broadside): Boolean ={
    if (body.isEmpty){
      log.error("Collector body not specified yet.")
      false
    }
    else{
      body.get.setWorkload(workload)
      true
    }
  }

  def selfSerialize: Unit  = {
    writerActor ! WriterActor.InWriterActorCollectorBackup(this.body.get)

  }

  protected def selfDeserializeAttempt(filename: String): Unit = {
    val fin = new FileInputStream(filename)
    val oin = new ObjectInputStream(fin)
    val body: Option[CollectorBody] = Some(oin.readObject.asInstanceOf[CollectorBody])
    this.body = body
    body.get.parent = this
    oin.close()
    fin.close()

  }

  def selfDeserialize: Broadside = {
    try{
      selfDeserializeAttempt(config.getString("dmcfs.cluster.files.checkpoint-file"))
      log.info("Resurrected.")
    }
    catch{
      case e: Exception => {
        log.warning("Primary backup damaged, trying secondary.")

        try{
          selfDeserializeAttempt(config.getString("dmcfs.cluster.files.checkpoint-file")+".new")
          log.info("Resurrected.")
          selfSerialize
        }
        catch{
          case e: Exception => {
            log.error("Unable to deserialize.")
            null
          }
        }


      }
    }

    body.get.workload.get

  }

  def receive = {


    case Collector.InPartialStatus(status,id) => {
      try {
        log.info("Update received from worker:" + id)

        iterationCounter += 1
        body.get.handle(status)

        val backupFrequency = config.getInt("dmcfs.cluster.files.checkpoint-frequency")
        if ((backupFrequency>0) && (iterationCounter % backupFrequency == 0))
          selfSerialize
      }
      catch{
        case e: Exception => {

          e.printStackTrace()
          log.error(e.getMessage)
          val trace = e.getStackTrace
          for (line <- trace){
            log.error(line.toString)
          }
        }
      }
    }

    case Collector.InParameterUpdate(param,value) => {
      log.info("Received parameter update.")
      try{
        if (param=="ConvergenceCriterion"){
          val newBody = ConvergenceCriterion(value).getCollectorBody
          if (newBody.nonEmpty){
            newBody.get.setParent(this)
            body=newBody
            sender ! Collector.OutParameterUpdate(true)
          }
          else
            sender ! Collector.OutParameterUpdate(false)
        }
        else{
            sender ! Collector.OutParameterUpdate(false)
        }
      }
      catch{
        case e: Exception => {
          log.error(e.getMessage)
          e.printStackTrace()
          sender ! Collector.OutParameterUpdate(false)
        }
      }
    }

    case Collector.InStopCriterion(scFeatures,scInteractions) =>{
      try {
        body.get.setStopCriterionFeatures(scFeatures)
        body.get.setStopCriterionInteractions(scInteractions)
        sender ! Collector.OutParameterUpdate(true)
      }
      catch{
        case e: Exception => {
          log.error(e.getMessage)
          e.printStackTrace()
          sender ! Collector.OutParameterUpdate(false)
        }
      }
    }

    case Collector.InWorkload(workload) => {
      log.info("Received Workload")
      sender ! OutParameterUpdate(setupWorkload(workload))
    }

    case Collector.Resurrect => {
      log.info("Resurrecting collector.")
      val workload = selfDeserialize
      if (workload==null)
        sender ! Resurrected(null,false,false,0,null,null,null)
      else
        sender ! Resurrected(
          workload,
          body.get.collect,
          body.get.permute,
          body.get.perm_s,
          body.get.getConvergenceCriterion,
          body.get.getStopCriterionFeatures,
          body.get.getStopCriterionInteractions
        )
    }
  }
}


object Collector{
  case class InPartialStatus(status: PartialStatus, workerId: Int)
  case class InParameterUpdate(param: String, value: String)
  case class InStopCriterion(scFeatures: stopcriterions.StopCriterion, scInteractions: stopcriterions.StopCriterion)
  case class InWorkload(workload: Broadside)
  case class OutParameterUpdate(status: Boolean)
  case object Resurrect
  case class Resurrected(workload: Broadside,
                         collecting: Boolean,
                         permutations: Boolean,
                         perm_s: Int,
                         cc: ConvergenceCriterion.Enum,
                         scFeatures: stopcriterions.StopCriterion,
                         scInteractions: stopcriterions.StopCriterion
                        )
  case object OutWorkloadReceived
  case object OutConvergenceReached
  case object Init
}