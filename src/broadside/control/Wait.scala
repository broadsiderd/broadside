/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control


import scala.concurrent.Await
import akka.pattern.ask
import akka.util.Timeout
import akka.actor.ActorRef

/**
 * Created by luk on 10/17/15.
 */
object Wait {
  def wait[inMsg,outMsg](actor: ActorRef, message: inMsg, t: Timeout): outMsg = {
    implicit val timeout = t
    val future = actor ? message
    Await.result(future,timeout.duration).asInstanceOf[outMsg]
  }
}
