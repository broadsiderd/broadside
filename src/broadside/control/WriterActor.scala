/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control

import java.io.{ByteArrayOutputStream, File, FileOutputStream, ObjectOutputStream}

import akka.actor.Actor.Receive
import broadside.api.Status
import broadside.control.mailboxes.WriterActorMailboxSemantics
import akka.actor.{Actor, ActorLogging}
import akka.dispatch.RequiresMessageQueue
import com.typesafe.config.Config
import broadside.control.WriterActor.{InWriterActorCollectorBackup, InWriterActorHandle}
import broadside.utils.Logger

/**
  * Created by luk on 8/7/16.
  */
class WriterActor(config: Config) extends Actor with RequiresMessageQueue[WriterActorMailboxSemantics] with ActorLogging{
  log.info("Writer constructor called.")
  val checkpointFrequency = config.getInt("dmcfs.cluster.files.checkpoint-frequency")
  val checkpointFile = config.getString("dmcfs.cluster.files.checkpoint-file")

  val statusSerialization = config.getBoolean("dmcfs.cluster.files.status-serialize")
  val statusFile = config.getString("dmcfs.cluster.files.status-file")
/*
  val batchFiles = config.getBoolean("dmcfs.cluster.files.batch-files")
  val distanceFile = config.getString("dmcfs.cluster.files.ranking-distances")
  val riFile = config.getString("dmcfs.cluster.files.relative-importances")
  val idFile = config.getString("dmcfs.cluster.files.inter-dependencies")
*/



  if (checkpointFrequency>0)
    checkPath(checkpointFile)

  if (statusSerialization)
    checkPath(statusFile)
/*
  if (batchFiles){
    checkPath(distanceFile)
    checkPath(riFile)
    checkPath(idFile)
  }
*/

  log.info("Writer constructor completed.")

  def checkPath(path: String) = {
    val newFile = new File(path)
    if (!newFile.canWrite)
      log.error("Path not available: " + path)
  }

  def fileSwap(path: String) = {
    val newfile = new File(path+".new")
    val oldfile = new File(path)
    oldfile.delete()
    newfile.renameTo(oldfile)
  }


  def obj2file(path: String, obj: Object): Unit ={
    try{
      val fout = new FileOutputStream(path+".new")
      val oout = new ObjectOutputStream(fout)
      oout.writeObject(obj)
      oout.close()
      fout.close()
      fileSwap(path)
    }
    catch{
      case e: Exception => log.error(e.getMessage)
    }
  }
/*
  def batchReports(lastStatus: Status) = {
    val ranking = lastStatus.featureRanking

    val rankingWriter = new Logger(riFile + ".new")
    rankingWriter.clear

    for (entry <- ranking){
      rankingWriter.write(entry.feature + "\t" + entry.totalEffect + "\t" + entry.pval)
    }
    rankingWriter.close()
    fileSwap(riFile)


    val ids = lastStatus.interactionRanking
    val idWriter = new Logger(idFile + ".new")
    idWriter.clear

    for (entry <- ids){
      idWriter.write(entry.sourceFeature + "\t" + entry.targetFeature + "\t" + entry.id + "\t" + entry.pval)
    }
    idWriter.close
    fileSwap(idFile)
  }
*/

  override def receive: Receive = {
    case InWriterActorHandle(status) => {
      if (statusSerialization)
        obj2file(statusFile,status)
      /*
      if (batchFiles)
        batchReports(status)
        */
    }
    case InWriterActorCollectorBackup(collectorBody) => {
      obj2file(checkpointFile,collectorBody)
    }
  }
}


object WriterActor{
  case class InWriterActorHandle(status: Status)
  case class InWriterActorCollectorBackup(collectorBody: CollectorBody)
}