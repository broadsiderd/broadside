/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control


import java.io.{File, FileOutputStream, ObjectOutputStream}

import akka.remote.RemoteScope
import broadside.api.Status
import java.util.LinkedList
import java.util.concurrent.TimeoutException

import broadside.engines.Broadside
import com.typesafe.config.Config

import scala.collection.immutable.LinearSeq
import scala.collection.mutable.ListMap
import scala.concurrent.Await
import scala.concurrent.duration._
import akka.pattern.ask
import akka.util.Timeout
import akka.actor._
import akka.cluster.Cluster
import akka.cluster.ClusterEvent.{MemberRemoved, MemberUp, UnreachableMember}
import broadside.data.StatusFactory


/**
 * Created by luk on 10/14/15.
 */
class Master(config: Config) extends Actor with ActorLogging{

  log.info("Master's constructor called.")
  protected var slaveCounter = 0
  protected val slaves = new ListMap[Address,SlaveReference]
  protected var workload: Option[Broadside] = None
  protected var cc: Option[ConvergenceCriterion.Enum] = None
  protected var stopCriterionFeatures: Option[stopcriterions.StopCriterion] = None
  protected var stopCriterionInteractions: Option[stopcriterions.StopCriterion] = None
  protected var busy = false
  protected var permuting = false
  protected var willPermute = false
  protected var statusStack  = new LinkedList[Status]
  protected var collectorWorkload = 0

  protected val cluster = Cluster(context.system)

  log.info("Spawning Collector.")
  val collectorProps = Props(classOf[Collector],config)
  val collector = context.actorOf(collectorProps,"MCFSCollector")


  val nLocalWorkers = config.getInt("dmcfs.cluster.master-workers")
  val localWorkerPool = new Array[ActorRef](nLocalWorkers)
  //val localWorkerFlags = new Array[Boolean](nLocalWorkers).map(v => false)
  var localDataActor: Option[ActorRef] = None

  if (nLocalWorkers>0){
    log.info("Spawning DataActor.")
    val dataActorProps = Props(classOf[DataActor],config)
    localDataActor = Some(context.actorOf(dataActorProps,"MCFSDataActor0"))


    log.info("Spawning Workers.")
    for (i<-1 to nLocalWorkers){
      val workerProps = Props(classOf[Worker],config,i,collector,localDataActor.get)
      localWorkerPool(i-1) = context.actorOf(workerProps,"MCFSWorker" + i)
    }
  }


  log.info("Master's constructor completed.")


  override def preStart(): Unit = {
    cluster.subscribe(self,classOf[MemberUp])
    cluster.subscribe(self,classOf[MemberRemoved])
    cluster.subscribe(self,classOf[UnreachableMember])
  }

  override def postStop(): Unit = {
    cluster.unsubscribe(self)
  }




  protected def setupSlave(addr: Address): Boolean = {
    try{
      slaveCounter += 1
      val id = slaveCounter
      log.info("Setting up slave " + id + " at " + addr)
      val dataActorProps = Props(classOf[DataActor],config).withDeploy(Deploy(scope=RemoteScope(addr)))
      val slaveDataActor = context.actorOf(dataActorProps,"MCFSDataActor"+id.toString)

      val slaveWorkerCount = config.getInt("dmcfs.cluster.remote-workers")
      val slaveWorkers = new Array[ActorRef](slaveWorkerCount)

      for (i <- 1 to slaveWorkerCount){
        val workerProps = Props(classOf[Worker],config,id*1000+i,collector,slaveDataActor).withDeploy(Deploy(scope=RemoteScope(addr)))
        slaveWorkers(i-1) = context.actorOf(workerProps,"MCFSWorker"+ (id*1000+i) )
      }

      val slaveReference = new SlaveReference(slaveDataActor,slaveWorkers,id)
      slaves += ((addr,slaveReference))

      true
    }
    catch{
      case e: Exception => {
        e.printStackTrace()
        false
      }
    }
  }

  protected def detachSlave(addr: Address) = {
    slaves.remove(addr)
  }

  protected def setupLocalDataActor: Boolean = {
    try{
      //val dataActorAnswer: DataActor.OutSetDataSource = Wait.wait(localDataActor.get,DataActor.InSetDataSource(workload.get), Timeout(6000 seconds))
      localDataActor.get ! DataActor.InSetDataSource(workload.get)
      //dataActorAnswer.status
      true
    }
    catch{
      case e: TimeoutException => {
        log.error("No reply from DataActor")
        false
      }
    }
  }

  protected def setupRemoteDataActorAtSlave(addr: Address): Boolean = {
    val slave = slaves.get(addr).get
    log.info("Setting up Remote DataActor at slave " + slave.id + " (" + addr.toString + ")")

    try{
      //val dataActorAnswer: DataActor.OutSetDataSource = Wait.wait(slave.dataActor,DataActor.InSetDataSource(workload.get), Timeout(6000 seconds))
      //dataActorAnswer.status
      slave.dataActor ! DataActor.InSetDataSource(workload.get)
      true

    }
    catch{
      case e: TimeoutException => {
        log.error("No reply from Remote DataActor" + slave.id)
        false
      }
    }
  }

  protected def setupRemoteDataActors: Boolean = {
    var status = true
    for (addr <- slaves.keys){
      status = status && setupRemoteDataActorAtSlave(addr)
    }
    status
  }

  protected def switchLocalWorkers: Unit = {
    //val workload = this.workload.get
    //val workloads = workload.getInstances(nLocalWorkers)
    for (i <- 1 to nLocalWorkers) {
      if (busy && !permuting)         localWorkerPool(i - 1) ! Worker.InStart(workload.get.getInstance)
      else if (busy && permuting)     localWorkerPool(i - 1) ! Worker.InPermute(workload.get.getInstance)
      else                            localWorkerPool(i - 1) ! Worker.InStop
    }
  }


  protected def switchRemoteWorkersAtSlave(addr: Address): Unit = {
    var status = true
    val slave = slaves.get(addr).get
    log.info("Starting Remote Workers at slave " + slave.id + " (" + addr.toString + ")")
    //val workloads = this.workload.get.getInstances(slave.workers.size)

    for (i <- 1 to slave.workers.size){
      if (busy && !permuting)         slave.workers(i - 1) ! Worker.InStart(workload.get.getInstance)
      else if (busy && permuting)     slave.workers(i - 1) ! Worker.InPermute(workload.get.getInstance)
      else                            slave.workers(i - 1) ! Worker.InStop
    }

  }

  protected def switchRemoteWorkers = {
    for (addr <- slaves.keys){
      switchRemoteWorkersAtSlave(addr)
    }
  }

  protected def runAll = {
    var allOK = false

    if (nLocalWorkers>0){
      log.info("Setting up Local DataActor")
      allOK = setupLocalDataActor
    }

    if (nLocalWorkers>0){
      switchLocalWorkers
    }


    log.info("Setting up Remote DataActors")
    if (!setupRemoteDataActors)
      log.warning("Errors while setting up Remote DataActors (continuing).")


    log.info("Setting up Remote Workers")
    switchRemoteWorkers

  }





  def receive = {

    case MemberUp(m) => {
      if (m.roles.contains("SLAVE")){
        val status = setupSlave(m.address)
        if (status && busy){
          setupRemoteDataActorAtSlave(m.address)
          switchRemoteWorkersAtSlave(m.address)
        }
      }
    }

    case MemberRemoved(m,s) => detachSlave(m.address)
    case UnreachableMember(m) => detachSlave(m.address)

    case Master.InIsBusy => {
      sender ! Master.OutIsBusy(busy)
    }


    case Master.InStart(workload, cc, scFeatures, scInteractions) => {
      if (busy){
        sender() ! Master.OutStarted(false, "ERROR: System busy.")
      }
      else{



      busy = true
      permuting = false
      willPermute = if (workload.perm>0) true else false

        this.workload = Some(workload)
        this.cc = Some(cc)

        this.stopCriterionFeatures = Some(scFeatures)
        this.stopCriterionInteractions = Some(scInteractions)


      log.info("Preparing local actors.")

        var allOK = true

        log.info("Setting up Collector.")
        try{
          var collectorAnswer: Collector.OutParameterUpdate = null

          collectorAnswer  = Wait.wait(collector,Collector.InParameterUpdate("ConvergenceCriterion",cc.toString), Timeout(6000 seconds))
          if (collectorAnswer.status==false)
            allOK=false

          collectorAnswer  = Wait.wait(collector,Collector.InStopCriterion(scFeatures,scInteractions), Timeout(6000 seconds))
          if (collectorAnswer.status==false)
            allOK=false


          collectorAnswer = Wait.wait(collector,Collector.InWorkload(workload), Timeout(6000 seconds))
          if (collectorAnswer.status==false)
            allOK=false


        }
        catch{
          case e: TimeoutException => {
            log.error("No reply from Collector.")
            allOK = false
          }
        }


        runAll


        if (allOK){
          sender() ! Master.OutStarted(true,"All started.")
        }
        else{
          busy = false
          sender() ! Master.OutStarted(false, "Some workers were unable to start.")
        }
      }
    }

    case Master.InResurrect => {
        log.info("Attempting to resurrect:)")
        var allOK = true


        try {
          val status = Wait.wait(collector, Collector.Resurrect, Timeout(6000 seconds)).asInstanceOf[Collector.Resurrected]
          if (status.workload == null) {
            log.error("Unable to resurrect.")
            allOK = false
          }
          else {
            this.workload = Some(status.workload)
            this.cc = Some(status.cc)
            this.stopCriterionFeatures = Some(status.scFeatures)
            this.stopCriterionInteractions = Some(status.scInteractions)

            busy = status.collecting || status.permutations
            permuting = status.permutations

            if (workload.get.perm > 0) willPermute = true
            else willPermute = false
          }


          runAll

          if (allOK) {
            sender() ! Master.OutStarted(true, "All started.")
          }
          else {
            busy = false
            sender() ! Master.OutStarted(false, "Some workers were unable to start.")
          }

          }
        catch {
          case e: TimeoutException => {
            log.error("No reply from Collector.")
            sender ! Master.OutStarted(false, "ERROR: Unable to resurrect.")
          }
        }

    }


    case Worker.OutStatusUpdate(status,sender) => {

      if (status.permutations)
        log.info("Permutation status received.")
      else
        log.info("Status update received from Collector, s= " + status.s)


      if (status.completed){
        //just swtich to permutations
        if (willPermute && (!permuting)){
          permuting = true
          switchLocalWorkers
          switchRemoteWorkers
          status.permutationFraction = 0.0
        }
        //permutations
        else if (willPermute && permuting){
          busy = false
          permuting = false
          willPermute = false
          switchLocalWorkers
          switchRemoteWorkers
        }
        //finished main, won't permute
        else{
          busy = false
          permuting = false
          willPermute = false
          switchLocalWorkers
          switchRemoteWorkers
          status.permutationFraction = -1.0
        }
      }
      statusStack.add(status)

      if (this.cc.get.toString=="TWIN")
        collectorWorkload -= 2
      else
        collectorWorkload -= 1


      val workerCount = if (slaves.size==0) nLocalWorkers
                        else nLocalWorkers + slaves.map( host => host._2.workers.size).reduce((v1,v2)=>v1+v2)

      val queue2ResourceRatio = collectorWorkload.toDouble / workerCount.toDouble

      if (queue2ResourceRatio >= config.getDouble("dmcfs.cluster.queue-warning")){
        val errorMessage = "The number of messages for Collector is now " + collectorWorkload + ", that is " +  queue2ResourceRatio + " the number of Workers."
        val errStatus = StatusFactory.error(errorMessage)
        statusStack.add(errStatus)
        log.warning(errorMessage)
      }


    }

    case Master.InCollectorNotified => {
      collectorWorkload += 1
    }



    case Master.InGetStatusStack => {
      val sstack = new LinkedList[Status]
      while (!statusStack.isEmpty)
        sstack.add(statusStack.removeFirst)
      sender() ! Master.OutStatusStack(sstack)

    }

    case Master.InGetDataActor => {
      if (!localDataActor.isEmpty)
        sender ! Master.OutDataActor(Some(localDataActor.get))
      else if (slaves.size>0){
        val slaveIterator = slaves.iterator
        sender ! Master.OutDataActor(Some(slaveIterator.next._2.dataActor))
      }
      else
        sender ! Master.OutDataActor(None)
    }



  }



}

object Master{
  case object InIsBusy
  case class OutIsBusy(busy: Boolean)

  case class InStart(workload: Broadside, cc: ConvergenceCriterion.Enum, scFeatures: stopcriterions.StopCriterion, scInteractions: stopcriterions.StopCriterion)
  case object InResurrect
  case class OutStarted(status: Boolean, info: String)


  case object InGetStatusStack
  case class OutStatusStack(sstack: java.util.LinkedList[Status])

  case object InCollectorNotified

  case object InGetDataActor
  case class OutDataActor(dataActor: Option[ActorRef])
}
