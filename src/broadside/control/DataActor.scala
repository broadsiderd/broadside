/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control

import broadside.engines.Broadside
import broadside.data.{Dataset, Feature}
import com.typesafe.config.Config
import akka.actor.{Actor, ActorLogging}
import broadside.utils.Samplers

/**
  * Created by luk on 1/31/16.
  */
class DataActor(config: Config) extends Actor with ActorLogging {
  log.info("DataActor constructor called.")

  var ds: Option[Dataset] = None

  var features: Array[Feature] = null
  var decision: Feature = null
  var m = 0

  log.info("DataActor constructor completed.")




  def receive = {
    case DataActor.InSetDataSource(workload) => {
      try{
        log.info("Loading dataset.")
        ds = workload.dataset
        val loadStatus = ds.get.validate

        if (loadStatus.contains("ERROR")){
          log.error(loadStatus)
          sender ! DataActor.OutSetDataSource(false)
        }
        else {
          features = ds.get.getLocalInstance.get.filter(v => (v.isDecision != true))
          decision = ds.get.getLocalInstance.get.filter(v => (v.isDecision == true))(0)

          if (workload.m_fraction != 0)
            m = scala.math.ceil(workload.m_fraction * features.length.toDouble).toInt
          else
            m = workload.m_absolute

          log.info("Dataset loaded.")
          sender ! DataActor.OutSetDataSource(true)

        }

      }
      catch{
        case e: Exception => {
          log.error("Error while setting up the Data Actor.")
          e.printStackTrace()
          sender ! DataActor.OutSetDataSource(false)
        }
      }

    }

    case DataActor.InGetDecision => {
      sender ! DataActor.OutDecision(decision)
    }

    case DataActor.InGetSample => {
      val featureSubset = Samplers.permutation(Samplers.randomSubset(features,m).toArray)

      sender ! DataActor.OutSample(featureSubset)
    }

    case DataActor.InGetFeatureNames => {
      val featureNames = features.filter(v => v.isDecision==false).map(v=>v.name)
      sender ! DataActor.OutFeatureNames(featureNames)
    }
  }
}


object DataActor{
  case class InSetDataSource(workload: Broadside)
  case class OutSetDataSource(status: Boolean)
  case object InGetDecision
  case class OutDecision(decision: Feature)
  case object InGetFeatureNames
  case class OutFeatureNames(featureNames: Array[String])
  case object InGetSample
  case class OutSample(features: Seq[Feature])

}