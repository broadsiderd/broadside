/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

package broadside.control

import broadside.api.{FeatureEntry, InteractionEntry, Status}
import broadside.control.stopcriterions.StopCriterion
import broadside.data._
import broadside.engines.Broadside
import com.typesafe.config.{Config, ConfigFactory}
import broadside.statistics.SurvivalStatisticalTests

import scala.collection.mutable.{ArrayBuffer, ArraySeq, HashMap, ListBuffer}
import scala.collection.immutable.HashSet

/**
 * Created by luk on 11/23/15.
 */
abstract class CollectorBody extends Serializable{


  @transient var parent: Collector = null



  var workload: Option[Broadside] = None
  var collect = true
  var permute = false



  val config = ConfigFactory.load("defaults").getConfig("dmcfs.defaults")
  var p = config.getInt("p")
  var p_interactions = config.getInt("p_interactions")
  var feature_list_limit = config.getInt("feature_list_limit")
  var s_initial = config.getInt("s_initial")
  var s_delta = config.getInt("s_delta")
  var s_max = config.getInt("s_max")
  var s_min = config.getInt("s_min")
  var perm = config.getInt("perm")


  var oldFeatureRanking: Option[FeatureRanking] = None
  var newFeatureRanking: Option[FeatureRanking] = None
  var mainEffectList = new MainEffectList
  var oldInteractionRanking = new InteractionRanking
  var newInteractionRanking = new InteractionRanking
  var sOld = 0
  var sOldTarget = s_initial
  var sNew = 0
  var sNewTarget = sOldTarget + s_delta
  var distancesFeatures = new ArrayBuffer[Double]()
  var distancesInteractions = new ArrayBuffer[Double]()

  var permutationsCompleted = -1
  var fullFeatureList: Array[String] = null
  var finalRiList: Array[FeatureEntry] = null
  var finalIDList: Array[InteractionEntry] = null
  var riDistributions: ArrayBuffer[ArrayBuffer[Double]] = null
  var idDistributions: ArrayBuffer[ArrayBuffer[Double]] = null
  var perm_s = 0


  var stopCriterionFeatures: stopcriterions.StopCriterion = null
  var stopCriterionInteractions: stopcriterions.StopCriterion = null

  protected def recalculate

  protected def permutationsSetup = {
    specificPermutationSetup


    finalRiList = oldFeatureRanking.get.getNormalized(if (feature_list_limit==0 ) Int.MaxValue else feature_list_limit, Some(mainEffectList.ranking))
    finalIDList = oldInteractionRanking.byID(Int.MaxValue)
    riDistributions = new ArrayBuffer[ArrayBuffer[Double]](finalRiList.length)
    for (i<- 1 to finalRiList.length){
      riDistributions += (new ArrayBuffer[Double](perm)).map( v=> Double.NaN)
    }

    idDistributions = new ArrayBuffer[ArrayBuffer[Double]](finalIDList.length)
    for (i <- 1 to finalIDList.length){
      idDistributions += new ArrayBuffer[Double](perm)
    }
    permutationsCompleted = 0
  }


  def setParent(parent: Collector): Unit = {
    this.parent = parent
  }

  def setWorkload(workload: Broadside): Unit = {
    this.workload = Some(workload)
    p = workload.p
    p_interactions = workload.p_interactions
    feature_list_limit = workload.feature_list_limit
    s_initial = workload.s_initial
    s_delta = workload.s_delta
    s_max = workload.s_max
    s_min = workload.s_min
    perm = workload.perm
    recalculate
  }

  def setStopCriterionFeatures(stopCriterion: StopCriterion): Unit = {
    this.stopCriterionFeatures = stopCriterion
  }

  def setStopCriterionInteractions(stopCriterion: StopCriterion): Unit = {
    this.stopCriterionInteractions = stopCriterion
  }

  def getStopCriterionFeatures: stopcriterions.StopCriterion = this.stopCriterionFeatures

  def getStopCriterionInteractions: stopcriterions.StopCriterion = this.stopCriterionInteractions

  def getConvergenceCriterion: ConvergenceCriterion.Enum

  def handle(status: PartialStatus):Unit = {

    if (null==fullFeatureList){
      fullFeatureList = parent.provideFullFeatureList
    }


    if (status.technical){
      parent.info("Technical update received.")

      if (status.errorCondition){
        parent.warn("Worker was unable to setup: " + status.info)
      }

    }
    else if (status.permutations && permute){


      parent.info(
        "Received permutation update. Seconds consumed - TOTAL: " + status.timeTotal/1000000
        + " TRAINING: " + status.timeTrain/1000000
        + " SCORING FEATURES:" + status.timeScoreFeatures/1000000
        + " SCORING INTERACTIONS:" + status.timeScoreInteractions/1000000
      )

      if (permutationsCompleted == -1)
        permutationsSetup

      if (newFeatureRanking == None){
        sNew = s_delta
        sNewTarget = perm_s
        newFeatureRanking = Some(new FeatureRanking(status.partialTotalEffects,status.partialFeatureProjections))
        newInteractionRanking.flush
      }
      else{
        newFeatureRanking.get.addTotal(status.partialTotalEffects,status.partialFeatureProjections)
        sNew += s_delta
      }
      newInteractionRanking.add(status.partialInteractionEffects)


      if (sNew==sNewTarget){
        permutationsCompleted += 1
        val ris = newFeatureRanking.get.getNormalized(if (feature_list_limit==0) Int.MaxValue else feature_list_limit).map(v => v.totalEffect)
        val ids = newInteractionRanking.byID(Int.MaxValue).map(v=> v.id)

        for (i <- 1 to ris.length){
          if (i > (riDistributions.length)){
            riDistributions += new ArrayBuffer[Double]()
          }
          riDistributions(i-1) +=  ris(i-1)
        }


        for (i <-1 to ids.length){
          if (i > (idDistributions.length)){
            idDistributions += new ArrayBuffer[Double]()
          }
          idDistributions(i-1) += ids(i-1)
        }


        val feats = finalRiList.map(v=>v.feature).toSet

        val reportRIList=  finalRiList.zip(riDistributions).map(v => {
          val riEntry = v._1
          val permutations = v._2.clone()

          for (i <- 1 to perm - permutations.length){
            permutations += Double.NaN
          }

          riEntry.permutations = permutations.toArray
          riEntry.totalEffectZScore = SurvivalStatisticalTests.robustZScore(riEntry.totalEffect,permutations.filter(v => !v.isNaN).toArray)

          riEntry
        })

        val reportIDList = finalIDList.zip(idDistributions).filter( v=> {
          feats.contains(v._1.sourceFeature) && feats.contains(v._1.targetFeature)
        }).map(v => {
          val idEntry = v._1
          val permutations = v._2.clone()
          for (i <-1 to perm - permutations.length){
            permutations += Double.NaN
          }
          idEntry.permutations = permutations.toArray
          idEntry.id_zscore = SurvivalStatisticalTests.robustZScore(idEntry.id,permutations.filter(v => !v.isNaN).toArray)
          idEntry
        })




        val partialPermStatus = StatusFactory.perm(
          reportRIList,
          reportIDList,
          sNewTarget,
          fullFeatureList,
          distancesFeatures.toArray,
          distancesInteractions.toArray,
          permutationsCompleted.toDouble / perm.toDouble
        )

        parent.send(partialPermStatus)
        sNew = 0
        newFeatureRanking = None

        parent.info("Completed " + permutationsCompleted + "/" + perm + " permutations.")
      }


      if (permutationsCompleted >= perm){
        permute = false
        parent.info("All permutations completed.")
        val finalStatus = new Status
        finalStatus.technical = true
        finalStatus.completed = true
        finalStatus.permutations = true
        parent.send(finalStatus)
      }

    }
    else if (collect){
      parent.info("RI update received. Seconds consumed - TOTAL: " + status.timeTotal/1000000
      + " TRAINING: " + status.timeTrain/1000000
      + " SCORING FEATURES:" + status.timeScoreFeatures/1000000
      + " SCORING INTERACTIONS:" + status.timeScoreInteractions/1000000
      )
        specificInit(status)


      if (sNew==sNewTarget){



        distancesFeatures += FeatureRanking.distance(newFeatureRanking.get,oldFeatureRanking.get,p)
        distancesInteractions += InteractionRanking.distance(newInteractionRanking,oldInteractionRanking,p_interactions)



        val featureRanking = newFeatureRanking.get.getNormalized(if (feature_list_limit==0) Int.MaxValue else feature_list_limit,Some(mainEffectList.ranking))
        val feats = featureRanking.map(v=>v.feature).toSet
        val interactionRanking = newInteractionRanking.byID(Int.MaxValue).filter(v => (feats.contains(v.sourceFeature) && feats.contains(v.targetFeature)))

        val outStatus = StatusFactory.standard(
          featureRanking,
          interactionRanking,
          sNew,
          fullFeatureList,
          distancesFeatures.toArray,
          distancesInteractions.toArray
        )


        perm_s = sNew

        val stopFeatures = stopCriterionFeatures.calculate(distancesFeatures.toArray)
        val stopInteractions = stopCriterionInteractions.calculate(distancesInteractions.toArray)
        val stopMinimum = outStatus.s >= s_min


        val info =
          " feature convergence: " + stopFeatures.toString + " interaction convergence:" + stopInteractions + " minimal samples:" + stopMinimum


        if ( (stopFeatures && stopInteractions && stopMinimum) || outStatus.s >= s_max){
          outStatus.completed = true
          collect = false

          if (workload.get.perm>0) permute=true


          if (outStatus.s >= s_max){
            parent.info("Iteration limit.")
            outStatus.info = "S_LIMIT"
          }
          else {
            parent.info("Convergence reached.")
            outStatus.info = "CONVERGENCE"
          }
        }

        parent.info("Sending update, s=" + sNew + " fdist=" + outStatus.distance + " idist=" + outStatus.distanceInteractions + info)
        parent.send(outStatus)



        specificTurnFinish




      }


    }
    else {
      parent.info("Discarding update.")
    }
  }

  protected def specificInit(status: PartialStatus): Unit

  protected def specificTurnFinish: Unit

  protected def specificPermutationSetup: Unit




}
