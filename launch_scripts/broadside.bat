set JAVA_OPTS=-Xmx16g -Xms1g -Xss16M
java -Dfile.encoding=UTF-8 -classpath lib/*;etc;broadside.jar broadside.control.Entry %* > logs/%computername%/log.txt 2> logs/%computername%/error.txt
