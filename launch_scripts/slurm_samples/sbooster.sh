sbatch \
	--array=0-4 \
	-N 1 \
	--cpus-per-task=40 \
	--ntasks-per-node=1 \
	--partition=k40 \
	-o ~/slurm/booster.out \
	~/slurm/booster.sh
