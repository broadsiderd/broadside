/*
 * Copyright (c) 2015-2020 by Lukasz Krol.
 *
 * This file is part of Broadside.
 *
 * Broadside is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Broadside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Broadside.  If not, see <http://www.gnu.org/licenses/>.
 */

import broadside.data.DataType;
import broadside.statistics.SurvivalArrays;
import org.junit.jupiter.api.*;
import broadside.statistics.SurvivalStatisticalTests;
import broadside.statistics.SurvivalArrays4PetoPrentice;
import broadside.statistics.SurvivalStatisticalTests;
import java.util.LinkedList;
//import broadside.statistics.

public class Tests {


    @Test
    void testPetoPrenticeSeries(){


        double[] ev = {1, 2, 5,-3,10,-8,15,-9,-28,100,12,17,4,2};
        Double[] events = new Double[ev.length];
        for (int i=0;i<events.length;++i){
            events[i] = ev[i];
        }


        SurvivalArrays4PetoPrentice sa = new SurvivalArrays4PetoPrentice(events);

        double[] Q_expected = {
                13.000000000000002,
                11.481727574750833,
                8.287631252662177,
                5.422355880752077,
                4.635094215915045,
                2.6587769191437594,
                1.5461440122523755,
                0.7820093702655436,
                0.015640304284819065,
                0.4494139914564855,
                0.6913323018144619,
                3.7645168232608985,
                2.6395153774464113
        };
        Double[] Q_obtained = SurvivalStatisticalTests.petoPrenticeSeriesFast(events,sa,1,13)._1;
        Double[] p_obtained = SurvivalStatisticalTests.petoPrenticeSeriesFast(events,sa,1,13)._2;

        for (int i=0; i<Q_expected.length; ++i){
            Assertions.assertEquals(Q_expected[i],Q_obtained[i]);
        }

        LinkedList<java.lang.Double> ll1 = new java.util.LinkedList<Double>();
        LinkedList<java.lang.Double> ll2 = new java.util.LinkedList<Double>();

        ll1.add(1.0);
        ll1.add(2.0);
        ll1.add(5.0);
        ll1.add(-3.0);
        ll1.add(10.0);
        ll1.add(-8.0);
        ll1.add(15.0);
        ll2.add(-9.0);
        ll2.add(-28.0);
        ll2.add(100.0);
        ll2.add(12.0);
        ll2.add(17.0);
        ll2.add(4.0);
        ll2.add(2.0);


        LinkedList<LinkedList<java.lang.Double>> llx = new java.util.LinkedList<java.util.LinkedList<Double>>();
        llx.add(ll1);
        llx.add(ll2);

        Double Qx = SurvivalStatisticalTests.petoPrentice(llx)._1;
        Double px = SurvivalStatisticalTests.petoPrentice(llx)._2;


        Assertions.assertEquals(
                1.546144012252375,
                Qx
        );

        Assertions.assertEquals(
                p_obtained[6],
                px
        );


        LinkedList<java.lang.Double> llx1 = new java.util.LinkedList<Double>();
        LinkedList<java.lang.Double> llx2 = new java.util.LinkedList<Double>();
        LinkedList<java.lang.Double> llx3 = new java.util.LinkedList<Double>();


        llx1.add(1.0);
        llx1.add(2.0);
        llx1.add(5.0);
        llx1.add(-3.0);
        llx1.add(10.0);
        llx2.add(-8.0);
        llx2.add(15.0);
        llx2.add(-9.0);
        llx2.add(-28.0);
        llx2.add(100.0);
        llx3.add(12.0);
        llx3.add(17.0);
        llx3.add(4.0);
        llx3.add(2.0);

        LinkedList<LinkedList<java.lang.Double>> llxx = new java.util.LinkedList<java.util.LinkedList<Double>>();
        llxx.add(llx1);
        llxx.add(llx2);
        llxx.add(llx3);


        Qx = SurvivalStatisticalTests.petoPrentice(llxx)._1;
        px = SurvivalStatisticalTests.petoPrentice(llxx)._2;


        Assertions.assertEquals(
                Qx,
                7.217399465775169
        );


    }

    @Test
    void testPetoPrenticeNumVsCat(){
        int nEvents = 33;

        Double[] times = new Double[nEvents];
        Double[] covariate = new Double[nEvents];

        LinkedList<LinkedList<Double>> groups = new java.util.LinkedList<LinkedList<Double>>();
        LinkedList<Double> group1 = new java.util.LinkedList<Double>();
        LinkedList<Double> group2 = new java.util.LinkedList<Double>();
        groups.add(group1);
        groups.add(group2);

        for (int i=0; i < nEvents; ++i){
            times[i] = new Double(i+1);
            if ((i % 10) == 0 )
                times[i] = times[i] * 10;
            if (i < nEvents/2){
                group1.add(new Double(times[i]));
                covariate[i] = new Double(1);
            }
            else {
                group2.add(new Double(times[i]));
                covariate[i] = new Double(2);
            }
        }

        //1 raw test
        SurvivalArrays4PetoPrentice sa = new SurvivalArrays4PetoPrentice(times);
        Double[] pvSeries = SurvivalStatisticalTests.petoPrenticeSeriesFast(times, sa, 1, times.length-1)._2;
        Double pvGroup = SurvivalStatisticalTests.petoPrentice(groups)._2;


        Assertions.assertEquals(
                pvSeries[nEvents/2-1],
                pvGroup
        );



    }

    @Test
    void testMortality(){

        double[] ev = {1, 2, 5,-3,10,-8,15,-9,-28,100,12,17,4,2};
        Double[] events = new Double[ev.length];
        LinkedList<Double> eventsList = new java.util.LinkedList<Double>();
        for (int i=0;i<events.length;++i){
            events[i] = ev[i];
            eventsList.add(ev[i]);
        }

        Double[] eventsAll = new Double[100];
        for (int i=0;i<eventsAll.length;++i){
            eventsAll[i] = (double)i+1;
        }

        SurvivalArrays4PetoPrentice sa1 = new SurvivalArrays4PetoPrentice(events);
        SurvivalArrays sa2 = new SurvivalArrays(events);


        double mort1 = sa1.getMortality(eventsAll);
        double mort2 = sa2.getMortality(eventsAll);
        double mort3 = SurvivalStatisticalTests.calculateMortality(eventsList,eventsAll);

        Assertions.assertEquals(mort1,mort2,mort3);

    }
}
